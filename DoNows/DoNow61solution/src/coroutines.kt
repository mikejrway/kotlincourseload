import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlin.random.Random

fun main(){
    example1()
}

/**
 * Note that the body of the function is surrounded by runBlocking: this puts the initial code into
 * a coroutine so that we can use the delay function before you complete the exercise steps.
 * We will examine structured (or nested) coroutine behaviours later
 */
fun example1() = runBlocking {
//T surround the next two lines of code with a GlobalScope.launch block
//-
    GlobalScope.launch {
//+
        delay(100)
        print("World!")
//-
    }
//+
//T surround the next two lines of code with a runBlocking block
//-
    runBlocking {
//+
        print("Hello ")
        delay(500)
//-
    }
//+
}

