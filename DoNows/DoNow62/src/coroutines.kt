import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlin.random.Random

fun main(){
    example2()
}

/**
 * Note that the body of the function is surrounded by runBlocking: this puts the code into a coroutine
 */
fun example2() = runBlocking {

// TODO Store the return value from GlobalScope.launch into a val called job
     GlobalScope.launch{
        delay(100)
        print("World!")
    }

    print("Hello ")
// TODO Call job.join() to wait for the coroutine to finish

}

