data class Station( val location: String, val conditions: Conditions) {

    constructor() : this( "Home", Conditions())
    var reports: Array<String> = arrayOf()

    /**
     * This function overrides the + operator allowing us to add a
     * String to the list of reports within a weather station.
     * As can be seen from main() this supports both + and +=
     */
//T Step 1: Define a function called plus which takes a string called report as an argument and returns a Station
//-
    operator fun plus(report: String): Station {
//+
//T Step 2: Remove the comment around the next two lines: this is the logic for the plus() function.

//C+
        reports += report
        return this
//C-
//H These two lines should be inside your new function
//-
    }
//+


    override fun toString(): String{
        val output = StringBuilder()
        for(report in reports){
            output.append("$report, ")
        }
        return output.toString()
    }
}

fun main(){
    var station = Station()
//T  Step 3: Note that we are using the + character to add strings to the station. This will call your new function
    station = station + "Sunny at home"
    station += "Raining at work"

    println(station)
}

// Dummy Conditions
class Conditions()