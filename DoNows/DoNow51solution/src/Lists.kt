fun main(){
    val list: List<String> = listOf<String>("one", "two", "three")
    val idxTwo = list.indexOf("two")
    val listTwoThree = list.subList(1,3)
    listTwoThree.forEach{print("$it,")} // "two,three."

    println("\n.................")
//T Convert the list to a mutable list
    val mList = list.toMutableList()
//T Remove the comments from the next line
//C+
    mList.add("four")
//C-
    list.forEach{print("$it,")} // "one,two,three,four"
}