fun main(){

 val numbers = arrayOf(10,9,8,7,6,5,4,3,2,1)

    //T Create a for loop around the println statement
    //T The for loop should declare a loop variable called idx and use the range syntax to iterate through the values 0 to 5

//-
    for( idx in 0..5){
//+
        println(numbers[idx])
//-
    }
//+
}