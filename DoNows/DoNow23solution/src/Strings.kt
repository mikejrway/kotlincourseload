fun main(){

//T Add .trimMargin() to the end of the string
//X+ :::.trimMargin():::
    val aString = """
        |Here is
        |some stuff
            |that should print with new line chars
                |and is badly laid out in the code
                
    """.trimMargin()
//X-
    println(aString)
}