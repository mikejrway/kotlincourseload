import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlin.random.Random
import kotlin.system.measureTimeMillis

fun main(){
    asyncExample()
}

fun asyncExample() = runBlocking<Unit> {
    val runtime = measureTimeMillis {
// TODO Modify the code below by wrapping the function call in an async{} block
    val statusCode = mostCommonRequestFailure()
// TODO Modify the code below by wrapping the function call in an async{} block
    val ip = ipWithMostFailedRequests()
// TODO Modify the print statement to use the await() function to retrieve the values from the Defferred objects statusCode and ip
    println("Most common failure $statusCode. Most failures from = $ip")
    }
    println("Total run time: $runtime mS")
}

private suspend fun mostCommonRequestFailure(): Int{
    delay(100)
    return 500
}
private suspend fun ipWithMostFailedRequests(): String {
    delay(200)
    return "211.123.199.66"
}



