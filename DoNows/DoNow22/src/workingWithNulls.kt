
fun main(){
// TODO Step 1: Change String to String? to mark the helloStr variable as being nullable
 var helloStr: String = "Hi there"

    var string2 ="" // Type is implicitly non-nullable String

// TODO Final Step : set helloStr to null

// TODO Step 2: Note that there is now an error with helloStr.length  because helloStr is nullable
// TODO Step 3: Replace the . in helloStr.length with the safe operator ?.
    println("helloStr is ${helloStr.length} characters long")

// TODO Step 4: Notice that the line below does not compile due to helloStr being nullable
// TODO Step 5: Add the following code in the line before the println: if(helloStr != null)
        string2 = helloStr


    println("The copy of the helloStr variable contains: $string2")


}

