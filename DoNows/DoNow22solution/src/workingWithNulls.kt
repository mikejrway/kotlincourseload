
fun main(){
//T Step 1: Change String to String? to mark the helloStr variable as being nullable
//R var helloStr: String = "Hi there"
//-
    var helloStr: String? = "Hi there" //Nullable var
//+

    var string2 ="" // Type is implicitly non-nullable String

//T Final Step : set helloStr to null
//-
    helloStr = null
//+

//T Step 2: Note that there is now an error with helloStr.length  because helloStr is nullable
//T Step 3: Replace the . in helloStr.length with the safe operator ?.
//R    println("helloStr is ${helloStr.length} characters long")
//-
    println("helloStr is ${helloStr?.length} characters long")
//+

//T Step 4: Notice that the line below does not compile due to helloStr being nullable
//T Step 5: Add the following code in the line before the println: if(helloStr != null)
//-
    if(helloStr != null)
//+
        string2 = helloStr


    println("The copy of the helloStr variable contains: $string2")


}

