package com.ltree.kotlin

import kotlinx.coroutines.*
import kotlinx.html.*
import kotlinx.html.dom.append
import kotlinx.html.js.*
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLElement
import kotlin.browser.document

const val BUTTON_LOAD = "load"
const val BUTTON_BY_IP = "byIp"
const val BUTTON_BY_UA = "byUa"


fun main() {
    addButtonsToPage()

}

private fun addButtonsToPage() {


    (document.getElementById(BUTTON_LOAD) as HTMLButtonElement).onclick = {
        GlobalScope.launch {
            loadLogFilesButtonAction()
        }
    }

    (document.getElementById(BUTTON_BY_IP) as HTMLButtonElement).onclick = {
        GlobalScope.launch {
            reportByIP_ButtonAction()
        }
    }

    (document.getElementById(BUTTON_BY_UA) as HTMLButtonElement).onclick = {
        GlobalScope.launch {
            reportByUserButtonAgentAction()
        }
    }

    updateState(ViewState.INITIAL)
}


private suspend fun loadLogFilesButtonAction() {

    updateState(ViewState.BUSY)
    showError("")
    //xhrRequest("http://localhost:8888/loadLogsXXS") { response ->
    try {
        val response = httpGet("http://localhost:8888/loadLogs")
        console.log("LOG MESSAGE" + response)
        val result = JSON.parse<Result<FileResult>>(response)
        if (result.error == false && result.data != null) {
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
                ?.append {
                    table {
                        tr {
                            th { text("Files Processed") }
                            th { text("Total Log Lines") }
                        }
                        tr {
                            td { text(result.data.fileCount) }
                            td { text(result.data.logEntryCount) }
                        }
                    }
                }
            updateState(ViewState.LOADED)
        } else {
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
            showError(result.errorMessage)
            updateState(ViewState.INITIAL)
        }
    } catch (e: RuntimeException) {
        showError("Something went wrong : ${e.message}")
    }
    //}
}

/**
 * Button click handler for the report by IP button
 */
private suspend fun reportByIP_ButtonAction() {
    try {
        showError("")
        updateState(ViewState.BUSY)
        val response = httpGet("http://localhost:8888/filterByIPFreq")
        console.log("LOG MESSAGE" + response)
        val result = JSON.parse<Result<Array<ReqCount>?>>(response)

        if (result.data != null) {
            val resultList: List<ReqCount> = result.data.toList()
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
                ?.append {
                    createReqIP_Table(resultList)
                }
        } else {
            showError("No entries returned")
        }
        updateState(ViewState.LOADED)
    } catch (e: RuntimeException) {
        showError("Something went wrong : ${e.message}")
    }

}

private fun TagConsumer<HTMLElement>.createReqIP_Table(resultList: List<ReqCount>) {
    table {
        tr {
            th { text("Requesting IP") }
            th { text("# Requests") }
        }

        for (reqCount in resultList) {
            tr {
                td { text(reqCount.ipAddr as String) }
                td { text(reqCount.count) }
            }
        }
    }
}


/**
 * Button click handler for the report by User Agent button
 */
private suspend fun reportByUserButtonAgentAction() {
    try {
        showError("")
        updateState(ViewState.BUSY)
        val response = httpGet("http://localhost:8888/filterByUA")
        console.log("LOG MESSAGE" + response)
        val result = JSON.parse<Result<Array<UACount>?>>(response)

        if (result.data != null) {
            val resultList: List<UACount> = result.data.toList()
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
                ?.append {
                    createUA_Table(resultList)
                }
        } else {
            showError("No entries returned")
        }
        updateState(ViewState.LOADED)
    } catch (e: RuntimeException) {
        showError("Something went wrong : ${e.message}")
    }
}

/** Creates the table for the report by UA.
 * Uses the uacol and countcol classes to set the column widths
 */
private fun TagConsumer<HTMLElement>.createUA_Table(resultList: List<UACount>) {
    table {
        tr {
            th {
                classes += "uacol"
                text("User Agent")
            }
            th {
                classes += "countcol"
                text("# Requests")
            }
        }

        for (reqCount in resultList) {
            tr {
                td {
                    classes += "uacol"
                    text(reqCount.uaName as String)
                }
                td {
                    classes += "countcol"
                    text(reqCount.count)
                }
            }
        }
    }
}


/**
 * Implementation of a state-management pattern to control the enabling of buttons
 */
private fun updateState(state: ViewState) {
    when (state) {
        ViewState.INITIAL -> {
            disableButton(BUTTON_LOAD, false)
            disableButton(BUTTON_BY_IP, true)
            disableButton(BUTTON_BY_UA, true)
        }
        ViewState.LOADED -> {
            disableButton(BUTTON_LOAD, false)
            disableButton(BUTTON_BY_IP, false)
            disableButton(BUTTON_BY_UA, false)
        }
        ViewState.BUSY -> {
            disableButton(BUTTON_LOAD, true)
            disableButton(BUTTON_BY_IP, true)
            disableButton(BUTTON_BY_UA, true)
        }
    }

}

private fun disableButton(id: String, state: Boolean) {

    val but = document.getElementById(id) as HTMLButtonElement
    console.log("Updating button: $id")
    but?.disabled = state
    //(document.getElementById(id) as HTMLButtonElement?)?.disabled = state
}

private fun showError(error: String) {
    document.getElementById("errors")
        ?.also { it.innerHTML = "" }
        ?.append {
            h1 {
                text(error)
            }
        }
}

/**
 * UI states
 */
private enum class ViewState {
    INITIAL, LOADED, BUSY
}

