package com.ltree.kotlin

/**
 * Data classes for communication with the Web servlet as return values
 */
//data class Result(val fileCount: Int = 0, val logEntryCount: Int = 0, val error: Boolean = false, val errorMessage : String = "")
data class Result<T>(val error: Boolean = false, val errorMessage : String = "", val data: T? = null)
data class FileResult(val fileCount: Int = 0, val logEntryCount: Int = 0)
data class UACount(val uaName: CharSequence, val count:Int)
data class ReqCount(val ipAddr: CharSequence, val count:Int)