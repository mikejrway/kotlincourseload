package com.ltree.kotlin


interface ILogParser {

    fun loadLogEntries() : Result<FileResult>

    fun loadLogEntries( fileName: String ): Result<FileResult>

    fun filterLogsByUniqueRequestIP(): Result<List<ReqCount>>

    fun filterLogsByUserAgent(): Result<List<UACount>>

}
