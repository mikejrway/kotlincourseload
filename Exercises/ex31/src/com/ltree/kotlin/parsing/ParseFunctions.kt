// TODO Step 1: Add a package statement setting the package to com.ltree.kotlin.parsing

// TODO Step 2: Define a constant NO_IP = "No IP in entry". Supply an explict type of CharSequence

// TODO Step 3: Create a function called parseIPAddress. The function should take a single argument logLine which is a string
//// The function should return a CharSequence (the interface which represents a String) which will be the extracted IP address
// TODO Step 4: Create a variable called ipAddress of type CharSequence and initialise it with NO_IP
// TODO Step 5: Verify that the logLine has a length > 0 and that it does contain a space (indexOf(' ')).
// *HINT* There is a function on the String class called isNotEmpty() which might be helpful!
// TODO Step 6: If either condition is not true then return NO_IP

// TODO Step 7: Take the code from processLogEntry() in LogParser.kt which extracts the IP address and place it here
// Modify the code so that it returns the IP address instead of printing it


// ********************************************************
// TODO Stop here for now and refer back to the exercise manual
// ********************************************************
// TODO PART 2 Starts Here >>>

// TODO Step 20: Create a new function parseRequestURL which will satisfy the requirements of the testParseRequestURL() test
// The argument to the function should be a nullable String, the function should return a nullable CharSequence

// TODO Step 21: Define a variable: res of type CharSequence - it should be nullable.
// TODO Step 22: Initialise the variable to null
// TODO Step 23: Use an If test to check that the parameter to this function is not null and not empty ...

// TODO Step 24: Use indexOf() to find the position of  the first and second double quote (") in the logLine
// *HINT* There is a version of indexOf which takes a start position - you have to begin the search for the second quote mark
// *HINT* after the first one!
// TODO Step 25: Populate the res variable with the text between the two quotes
// *HINT* Use String.subSequence
// TODO Step 26:  End of if goes here

// TODO Step 27: Return res
