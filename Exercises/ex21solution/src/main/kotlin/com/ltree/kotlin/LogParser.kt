package com.ltree.kotlin

const val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry2="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry3="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry4="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry5="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry6="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry7="""95.29.198.15 - - [12/Dec/2015:18:32:10 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""


fun main(){
    //T Create an array called logFileLines from the constants above
    //-
    val logFileLines = arrayOf(entry1, entry2,entry3,entry4,entry5,entry6,entry7 )
    //+

    //T Create a for loop to iterate through the log file lines
    //T Inside the for loop: call the function processLogEntry passing the logEntry as the only parameter
    //-
    for(logLine in logFileLines){
        processLogEntry(logLine)
    }
    //+
    println("Processing Complete")
}


//T Note that we have created a function called processLogEntry taking a single parameter called logEntry of type String
//C+
// you will need to complete the tasks within the function
//C-

fun processLogEntry( logLine: String){

//T Use the indexOf function of String to find the first occurrence of a space (' ') in the logEntry assign to a value called firstSpace
//-
    val firstSpace = logLine.indexOf(' ')
//+
//T Use the subSequence function of String to extract a sequence from logEntry which starts at 0
// and ends at the the position of the first space. Assign the result to a value of ipAddress
//-
    val ipAddress = logLine.subSequence(0, firstSpace)
//+
//T Print the ip address (ipAddress)
//-
    println(ipAddress)
//+

}

