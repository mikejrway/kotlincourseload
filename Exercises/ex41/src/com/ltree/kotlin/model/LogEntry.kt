// TODO Step 1 Add a package declaration of com.ltree.kotlin

// TODO Step 2 create a new class called LogEntry

// TODO Step 3 add the import for the class java.time.LocalDateTime

// TODO Step 4 Add a primary constructor taking the following arguments: ipAddr,  dateTime, requestURL, userAgent, referrer
// TODO The arguments should all be of type CharSequence except for dateTime which is java.time.LocalDateTime
// TODO and statusCode which should be an Int

// TODO Step 5 make the parameters into read-only properties of the class by adding val as a modifier to each property


// TODO Step 6 Provide a no argument constructor which calls the main constructor providing parameters as follows
// TODO ipAddr = "10.1.1.1", dateTime = LocalDateTime.of(1970, Month.JANUARY, 1, 1,1), requestURL="localhost",  responseCode=200, userAgent="Chrome"
// *HINT* You do not need to use named arguments (but you may if you wish)

// TODO Step 7 Add a data modifier to the class
