package com.ltree.kotlin.model

import java.time.LocalDateTime
import java.time.Month
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
//12/Dec/2015:18:25:11 +0100
val dateFormat = DateTimeFormatter.ofPattern("dd/MMM/yyyy:kk:mm:ss XXXX")

/**
 * A simple data class to represent a log entry.
 * You will create this class in a later exercise
 */
data class LogEntry(val ipAddr: CharSequence = "",
                    val dateTime: LocalDateTime = LocalDateTime.of(2020, Month.APRIL, 1, 1,1),
                    val requestURL: CharSequence = "",
                    val responseCode: Int= 200,
                    val userAgent: CharSequence= "",
                    val referrer: CharSequence = "") {
    /**
     * Default constructor with all properties defaulted
     */
    constructor(): this("", LocalDateTime.now(), "", 0 ,"","")
}

/**
 * A helper function to parse the time stamp CharSequence
 */
fun parseTimeStamp(timestamp: CharSequence): ZonedDateTime{
    return ZonedDateTime.parse(timestamp, dateFormat)
}
