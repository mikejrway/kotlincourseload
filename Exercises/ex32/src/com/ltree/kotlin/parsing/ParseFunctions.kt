package com.ltree.kotlin.parsing

const val NO_IP = "No IP in entry"

/**
 * Parses a log entry string and extracts the IP address from it
 */
fun parseIPAddress( logLine: String): CharSequence {
    // Create a variable called ipAddress of type CharSequence and initialise it with NO_IP

    var ipAddress: CharSequence = NO_IP

    // Verify that the logLine has a length > 0 and that it does contain a space (indexOf(' ')).
    // If either condition is not true then return NO_IP

    ipAddress = NO_IP
    if(logLine.isNotEmpty()) {
        val firstSpace = logLine.indexOf(' ')
        ipAddress = logLine.subSequence(0, firstSpace)
    }
    return ipAddress
}

    // PART 2 Starts Here >>>

    // Create a new function parseRequestURL which will satisfy the requirements of the testParseRequestURL() test
    // The argument to the function should be a nullable String, the function should return a nullable CharSequence

fun parseRequestURL(logLine: String?): CharSequence? {

    // Define a variable: res of type CharSequence - it should be nullable.
    // Initialise the variable to null

    var res: CharSequence? = null

    // Use an If test to check that the parameter to this function is not null and not empty ...

    if(logLine != null && logLine.isNotEmpty()) {


    // Use indexOf() to find the position of  the first and second double quote (") in the logLine
        val firstQuote = logLine.indexOf('"')
        val secondQuote = logLine.indexOf('"', firstQuote + 1)
    // Populate the res variable with the text between the two quotes

        res = logLine.subSequence(firstQuote + 1, secondQuote)

    }

    return res
}
