package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
import khttp.get
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import mu.KotlinLogging
import java.io.File
import java.io.StringReader
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.concurrent.TimeUnit
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

const val INPUT_FILE = """../../sampledata/small_access.log"""
const val SERVICE_URL = "http://localhost:9090/logserver/"

class LogParser : ILogParser {

    internal val logFileLines: MutableList<String> = ArrayList()

    private val logEntries:MutableList<LogEntry> = ArrayList()

//T Step 10: Create a Channel of type LogEntry assign it to val channel
//-
    val channel = Channel<LogEntry>()
//+


    override fun processLogEntries() {
        println("Loading Log Files")
//T Step 11: Create a GlobalScope.launch block save the job reference in a val receiverJob
//T Step 12: Inside the launch block: call channelReceiver() -- you will write this function soon
//-
        val receiverJob = GlobalScope.launch{
            channelReceiver()
        }
//+
        var elapsedTime = 0L

        elapsedTime = measureTimeMillis {

//T Step 3: Add runBlocking blocking block enclosing the for loop

//-
        runBlocking {
//+
//T Step 1: Enclose the call to loadLogEntriesFromServer with a for loop iterating the variable i from 0..10. T

//T Step 2: Replace the fileName argument with "split$i.log"
//-
                for (i in 0..10) {
//+

//T Step 6: Surround loadLogEntriesFromServer() (just the code inside the for loop) with a launch block
//-
                    launch{
//+

//R                        loadLogEntriesFromServer(INPUT_FILE)
//-
                        loadLogEntriesFromServer("split$i.log")
//+
//-
                    } // End of launch block
                }
            } // End runBlocking
//+
        } // End measure time block

        // Execution only gets to here once all the coroutines launched in runBlocking have completed
//T Step 13: Close the channel
//-
        channel.close()
//+
//T Step 14: Create a runBlocking block. Inside the block call receiverJob.join() to wait for the channel receiver to finish
//-
        runBlocking {
            receiverJob.join()
        }
//+

        val filteredLogEntries = getUniqueRequestIPs(logEntries)
        filteredLogEntries.forEach { println(it) }

        println("Processing Complete ${logEntries.size} entries processed.")
        val seconds = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.MILLISECONDS);
        println("Elapsed time: $seconds (seconds) $elapsedTime (milliseconds)")
    }

    //T Step 15: Create a new suspending function called channelReceiver()
    //T Step 16: Inside the function call channel.consumeEach
    //H The channel contains LogEntry objects
    //T Step 17: In the lambda block add the new LogEntry to logEntries
//-
    private suspend fun channelReceiver(){
        channel.consumeEach { logEntries.add(it) }
    }
//+

    /**
     * Load log files from an HTTP server
     */
//T Step 4: Add suspend modifier
//R     private fun loadLogEntriesFromServer( fileName: String ){
//-
    private suspend fun loadLogEntriesFromServer( fileName: String ){
        println("Loading file $fileName")
//+

        val paramMap = mapOf("fileName" to fileName)
//T Step 5: Surround the remaining code in this function with a withContext(Dispatchers.IO) block
//-
        withContext(Dispatchers.IO) {
//+
            val response = get(SERVICE_URL, params = paramMap)
            val reader = StringReader(response.text)

            reader.use {
                    reader ->
                reader.forEachLine {
                        line ->
//T Step 18: Wrap the line below in a launch {} block
//T Step 19: Modify the code in the launch block to send the LogEntry to the channel instead of adding it to logEntries
//H Objects are sent to a channel
//R                 logEntries.add(processLogEntry(line))
//-
                    launch {
                        channel.send(processLogEntry(line))
                    }
//+
                }
            }
//-
        }// end withContext
//+
    }

    /**
     * Load the log entries from a file
     */
    internal fun loadLogEntries( fileName: String ){
        File(fileName).reader().use {
            it.forEachLine {
                logFileLines.add(it)
            }
        }
    }

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    internal fun processLogEntry( logLine: String): LogEntry {
        val ipAddress = parseIPAddress(logLine)
        val url = parseRequestURL(logLine)
        return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
    }

    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
    internal fun getUniqueRequestIPs(logEntries: MutableList<LogEntry>): List<LogEntry>{
        return logEntries.distinctBy { it.ipAddr }
    }

} // End of the class

