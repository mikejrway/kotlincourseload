package com.ltree.kotlin

import kotlinx.coroutines.runBlocking
import mu.KotlinLogging



val logger = KotlinLogging.logger {}

fun main(){

    try {

        LogParser().processLogEntries()

    } catch (e: Exception) {
        println("Something horrible happened. See the log file for details")
        logger.error(e) { "Error processing log file lines" }

    }

}