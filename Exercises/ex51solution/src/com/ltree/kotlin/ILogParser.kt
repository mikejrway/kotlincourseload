package com.ltree.kotlin

interface ILogParser {
    fun processLogEntries()
}