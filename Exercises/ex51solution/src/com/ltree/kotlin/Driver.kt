package com.ltree.kotlin

import mu.KotlinLogging


//T - Bonus Step 1: Note that we have initialised a logger for you
val logger = KotlinLogging.logger {}

fun main(){

    //T Bonus Step 2: Add exception handler (try / catch) around the call to processLogEntries
//-
    try {
//+
        LogParser().processLogEntries()
//-
    } catch (e: Exception) {
//+
//T - Bonus Step 3: In the catch block print a message to the user
//H Perhaps : "A serious error has occurred. See the log file for details"
//-
        println("A serious error has occurred. See the log file for details")
//+
//T - Bonus Step 4: Use the logger to output a message
//H logger.error takes the exception and a lambda function (the last statement is the return value which then gets logged)
//-
        logger.error(e) { "Error processing log file lines" }

    }
//+
}