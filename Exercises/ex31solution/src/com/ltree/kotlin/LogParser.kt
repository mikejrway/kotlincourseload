package com.ltree.kotlin
//-
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
//+

const val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry2="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry3="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry4="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry5="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry6="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry7="""95.29.198.15 - - [12/Dec/2015:18:32:10 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""


fun main(){
    // Create an array called logFileLines from the constants above
    val logFileLines = arrayOf(entry1, entry2,entry3,entry4,entry5,entry6,entry7 )

    // Create a for loop to iterate through the log file lines
    // Inside the for loop: call the function processLogEntry passing the logEntry as the only parameter

    for(logLine in logFileLines){
        processLogEntry(logLine)
    }
    println("Processing Complete")
}


/**
 * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
 */
fun processLogEntry( logLine: String){
//T Step 10: Remove all the existing code but not the TODO statements
//T Step 11: Replace the code with a call to parseIPAddress() passing logLine as the argument

// CODE TO MOVE TO FUNCTION STARTS HERE >>>>>>>>>>>
//-
/*
//+

    // Use the indexOf function of String to find the first occurrence of a space (' ') in the logEntry assign to a value called firstSpace
    val firstSpace = logLine.indexOf(' ')

    // Use the subSequence function of String to extract a sequence from logEntry which starts at 0
    // and ends at the the position of the first space. Assign the result to a value of ipAddress

    val ipAddress = logLine.subSequence(0, firstSpace)
    // Print the ip address (ipAddress)
    println(ipAddress)
// CODE TO MOVE ENDS HERE <<<<<<<<<<<
//-
*/
//+
//T Step 12: Print the value returned from parseIPAddress
//-
    println(parseIPAddress(logLine))
//+

//T Step : When you get to Part 2:
// Call parseRequestURL and print the result. Pass logLine as the argument
//-
    println(parseRequestURL(logLine))
//+

}

