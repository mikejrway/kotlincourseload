//T Step 1: Add a package statement setting the package to com.ltree.kotlin.parsing
//-
package com.ltree.kotlin.parsing
//+

//T Step 2: Define a constant NO_IP = "No IP in entry". Supply an explict type of CharSequence
//-
const val NO_IP = "No IP in entry"
//+

//T Step 3: Create a function called parseIPAddress. The function should take a single argument logLine which is a string
//C+
// The function should return a CharSequence (the interface which represents a String) which will be the extracted IP address
//C-
//-
fun parseIPAddress( logLine: String): CharSequence {
//+
//T Step 4: Create a variable called ipAddress of type CharSequence and initialise it with NO_IP
//-
    var ipAddress: CharSequence = NO_IP
//+
//T Step 5: Verify that the logLine has a length > 0 and that it does contain a space (indexOf(' ')).
//H There is a function on the String class called isNotEmpty() which might be helpful!
//T Step 6: If either condition is not true then return NO_IP

//T Step 7: Take the code from processLogEntry() in LogParser.kt which extracts the IP address and place it here
// Modify the code so that it returns the IP address instead of printing it

//-

    if(logLine.isNotEmpty()) {
        val firstSpace = logLine.indexOf(' ')
        ipAddress = logLine.subSequence(0, firstSpace)
    }
    return ipAddress
//+
//-
}
//+

// ********************************************************
//T Stop here for now and refer back to the exercise manual
// ********************************************************
//T PART 2 Starts Here >>>

//T Step 20: Create a new function parseRequestURL which will satisfy the requirements of the testParseRequestURL() test
// The argument to the function should be a nullable String, the function should return a nullable CharSequence
//-
fun parseRequestURL(logLine: String?): CharSequence? {
//+

//T Step 21: Define a variable: res of type CharSequence - it should be nullable.
//T Step 22: Initialise the variable to null
//-
    var res: CharSequence? = null
//+
//T Step 23: Use an If test to check that the parameter to this function is not null and not empty ...
//-
    if(logLine != null && logLine.isNotEmpty()) {
//+

//T Step 24: Use indexOf() to find the position of  the first and second double quote (") in the logLine
//H There is a version of indexOf which takes a start position - you have to begin the search for the second quote mark
//H after the first one!
//-
        val firstQuote = logLine.indexOf('"')
        val secondQuote = logLine.indexOf('"', firstQuote + 1)
//+
//T Step 25: Populate the res variable with the text between the two quotes
//H Use String.subSequence
//-
        res = logLine.subSequence(firstQuote + 1, secondQuote)
//+
//T Step 26:  End of if goes here
//-
    }
//+

//T Step 27: Return res
//-
    return res
}
//+