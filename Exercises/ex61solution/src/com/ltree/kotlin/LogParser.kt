package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
import khttp.get
import mu.KotlinLogging
import java.io.File
import java.io.StringReader


const val INPUT_FILE = """split0.log"""

class LogParser : ILogParser {

    internal val logFileLines: MutableList<String> = ArrayList()

    private val logEntries:MutableList<LogEntry> = ArrayList()

    override fun processLogEntries(){
//T Step 10: Change this to call loadLogEntriesFromServer
//R         loadLogEntries(INPUT_FILE)
//-
        loadLogEntriesFromServer(INPUT_FILE)
//+

        val filteredLogEntries = getUniqueRequestIPs(logEntries)
        filteredLogEntries.forEach { println(it)}

        println("Processing Complete ${logEntries.size} entries processed")
    }


//T Step 1: Create a function to load the log files from a server - call it loadLogEntriesFromServer with a Unit return
//T Step 2: The function should take a parameter called fileName of type String
//-
    fun loadLogEntriesFromServer( fileName: String ){
//+
//T Step 3: Create a variable called params assign a map which maps "fileName" to the fileName parameter of this function
//H mapOf("fileName" to fileName)
//H Don't forget that all the necessary code has been discussed in the lecture session. Take a look at the slides...
//-
        val paramMap = mapOf("fileName" to fileName)
//+

//T Step 4: Use khttp.get() to send a get request to  "http://localhost:9090/logserver/" passing the params object as the second argument
//T Step 5: Assign the result of the get() call to a variable called response
//-
        val response = get("http://localhost:9090/logserver/", params=paramMap)
//+
//T Note: the response data will be in response.text

//T Step 6: Create a StringReader around the response text assign it to a new variable reader
//-
        val reader = StringReader(response.text)
//+
//T Step 7: Create a reader.use{} block
//-
        reader.use {
//+
            //T Step 8: Inside the {} for the lambda: put it.forEachLine {}
//-
                it.forEachLine {
//+
            //T Step 9: Inside the inner lambda block specify a parameter of line
//-
                line -> logEntries.add(processLogEntry(line))
//+
// In the body of the lambda put: logEntries.add(processLogEntry(line))    // Code from the file based loadLog function
//-
        }
        }
//+
//H The previous few steps are quite hard to describe so we've put the actual code in the exercise manual for you

//-
    }
//+

//T Don't forget to go back and do step 10
    /**
     * Load the log entries from a file
     */
    internal fun loadLogEntries( fileName: String ){
        File(fileName).reader().use {
            it.forEachLine {
                logFileLines.add(it)
            }
        }
    }

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    internal fun processLogEntry( logLine: String): LogEntry {
        val ipAddress = parseIPAddress(logLine)
        val url = parseRequestURL(logLine)
        return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
    }

    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
    internal fun getUniqueRequestIPs(logEntries: MutableList<LogEntry>): List<LogEntry>{
        return logEntries.distinctBy { it.ipAddr }
    }

} // End of the class

