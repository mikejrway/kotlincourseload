//T Step 1 Add a package declaration of com.ltree.kotlin
//-
package com.ltree.kotlin.model
import java.time.LocalDateTime
import java.time.Month

//+

//T Step 2 create a new class called LogEntry

//T Step 3 add the import for the class java.time.LocalDateTime

//T Step 4 Add a primary constructor taking the following arguments: ipAddr,  dateTime, requestURL, userAgent, referrer
//T The arguments should all be of type CharSequence except for dateTime which is java.time.LocalDateTime
//T and statusCode which should be an Int

//T Step 5 make the parameters into read-only properties of the class by adding val as a modifier to each property


//-
class LogEntry(val ipAddr: CharSequence,
                    val dateTime: LocalDateTime = LocalDateTime.of(2020, Month.APRIL, 1, 1,1),
                    val requestURL: CharSequence = "",
                    val responseCode: Int= 200,
                    val userAgent: CharSequence= "",
                    val referrer: CharSequence = "")

{
//+
//T Step 6 Provide a no argument constructor which calls the main constructor providing parameters as follows
//T ipAddr = "10.1.1.1", dateTime = LocalDateTime.of(1970, Month.JANUARY, 1, 1,1), requestURL="localhost",  responseCode=200, userAgent="Chrome"
//H You do not need to use named arguments (but you may if you wish)
//-
    constructor() : this("10.1.1.1",
                            LocalDateTime.of(1970, Month.JANUARY, 1, 1,1),
                    "localhost",
                    200,
                        "Chrome",
                        "")
//+
//-
}
//+

//T Step 7 Add a data modifier to the class