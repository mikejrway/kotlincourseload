package com.ltree.kotlin.model

import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.Month

internal class LogEntryTest {

    @BeforeEach
    fun setUp() {
    }

    @Test
    fun testCreation(){
        val subject = LogEntry("10.1.1.99")
        val expected = LogEntry(ipAddr = "10.1.1.99",
        dateTime = LocalDateTime.of(2020, Month.APRIL, 1, 1,1),
        requestURL= "",
        responseCode= 200,
        userAgent= "",
        referrer = "")

        assertEquals(expected, subject)
    }

    @Test
    fun testCreation_DefaultConstructor(){
        val subject = LogEntry()
        val expected = LogEntry(ipAddr = "10.1.1.1",
            dateTime = LocalDateTime.of(1970, Month.JANUARY, 1, 1,1),
            requestURL= "localhost",
            responseCode= 200,
            userAgent= "Chrome",
            referrer = "")

        assertEquals(expected, subject)
    }

}
