package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL

const val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry2="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry3="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry4="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry5="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry6="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry7="""95.29.198.15 - - [12/Dec/2015:18:32:10 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""


// TODO Step 1: Create a class called LogParser wrap all the code in the file inside the class brackets


//+
// TODO Step 2: rename the main function to processLogEntries
 fun main(){

// TODO Step 3:  Move these two array declarations  to be fields of the class and mark them private
// *HINT* They do not need to be fields of the class yet but things will change in subsequent exercises
// *HINT* They need to be outside of the function but inside the class

    val logFileLines = arrayOf(entry1, entry2,entry3,entry4,entry5,entry6,entry7 )
    val logEntries: Array<LogEntry> = Array(logFileLines.size){ LogEntry() }

    var index = 0
    for(logLine in logFileLines){
        logEntries[index++] = processLogEntry(logLine)
    }


    val filteredLogEntries = getUniqueRequestIPs(logEntries)
    filteredLogEntries.forEach { println(it)}

    println("Processing Complete 1")
}


    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    // TODO Step 4: Make this function internal (a good way of reducing visibility but still making the code available to test)
     fun processLogEntry( logLine: String): LogEntry {
        val ipAddress = parseIPAddress(logLine)
        val url = parseRequestURL(logLine)
        return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
    }

    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
    // TODO Step 5: Make this function internal
     fun getUniqueRequestIPs(logEntries: Array<LogEntry>): Array<LogEntry>{
        val distinctLogEntries =  logEntries.distinctBy { it.ipAddr }
        return distinctLogEntries.toTypedArray()
    }
