plugins {
    kotlin("multiplatform") version "1.3.60"
}

kotlin {
    jvm()
    js {
        browser()
    }
}


repositories {
    jcenter()
    mavenCentral()
}
val group = "com.example"
val version = "0.0.1"
val kotlinVersion = "1.3.61"
val ktorVersion = "1.2.2"
val kotlinCoroutineVersion = "1.3.3"

kotlin.sourceSets["jvmMain"].dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("khttp:khttp:1.0.0")
    implementation("io.github.microutils:kotlin-logging:1.7.6")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.3")
    implementation("org.slf4j:slf4j-api:1.7.26")
    implementation("org.slf4j:slf4j-log4j12:1.7.28")
    implementation( "org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-html-builder:$ktorVersion")
    implementation("io.ktor:ktor-gson:$ktorVersion")
}

kotlin.sourceSets["jvmTest"].dependencies {
        implementation( kotlin("test"))
        implementation( kotlin("test-junit"))
 }
kotlin.sourceSets["jsMain"].dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains.kotlinx:kotlinx-html-js:0.6.12")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:1.3.3")
    implementation(npm("jquery", "3.4.1"))
}
kotlin.sourceSets["commonTest"].dependencies{
        implementation(kotlin("test-common"))
        implementation(kotlin("test-annotations-common"))
}
kotlin.sourceSets["commonMain"].dependencies {

}


val run by tasks.creating(JavaExec::class) {
    group = "application"

    main = "com.ltree.kotlin.DriverKt"
    kotlin {
        val main = targets["jvm"].compilations["main"]
        dependsOn(main.compileAllTaskName)
        classpath(
            { main.output.allOutputs.files },
            { configurations["jvmRuntimeClasspath"] }
        )
    }
    ///disable app icon on macOS
    systemProperty("java.awt.headless", "true")
}

