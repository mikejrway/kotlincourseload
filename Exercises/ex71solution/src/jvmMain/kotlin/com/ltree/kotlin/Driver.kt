package com.ltree.kotlin

import kotlinx.coroutines.runBlocking
import mu.KotlinLogging

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import io.ktor.html.respondHtml
import io.ktor.http.CacheControl
import io.ktor.http.HttpStatusCode
import io.ktor.response.cacheControl
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.body
import kotlinx.html.h1
import kotlinx.html.img
import java.awt.image.BufferedImage


val logger = KotlinLogging.logger {}

fun main() {

    try {
        val driver = Driver()

        driver.runServer()


    } catch (e: Exception) {
        println("Something horrible happened. See the log file for details")
        logger.error(e) { "Error processing log file lines" }

    }

}

class Driver {
    val logParser = LogParser()



    fun runServer() {

        // Ktor HTTP server startup
        val server = embeddedServer(Netty, host = jvmHost, port = jvmPort) {
            install(CORS) {
                anyHost()
            }
            install(ContentNegotiation) {
                gson {
                    // Configure Gson here
                }
            }
            install(DefaultHeaders)
            install(CallLogging)
            install(Routing) {
                // Load the log files
                get("/loadLogs") {

                    this.call.response.cacheControl(CacheControl.NoCache(CacheControl.Visibility.Public))
                    call.respond(logParser.loadLogEntries())

                }
                get("/loadLogFile") {
                    val fileName: String? = call.request.queryParameters["fileName"]
                    this.call.response.cacheControl(CacheControl.NoCache(CacheControl.Visibility.Public))
                    if(fileName != null) {
                        call.respond(logParser.loadLogEntries(fileName))
                    } else {
                        call.respond(logParser.loadLogEntries())
                    }
                }
                get("/filterByIPFreq") {
                    call.response.cacheControl(CacheControl.NoCache(CacheControl.Visibility.Public))
                    call.respond(logParser.filterLogsByUniqueRequestIP())

                }

                get("/filterByUA") {
                    call.response.cacheControl(CacheControl.NoCache(CacheControl.Visibility.Public))
                    call.respond(logParser.filterLogsByUserAgent())

                }



            }
        }

        server.start()
    }
}