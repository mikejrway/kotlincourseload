package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.buildLogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
import com.ltree.kotlin.parsing.tokeniseLine
import mu.KotlinLogging
import java.io.File
import kotlin.system.measureTimeMillis


const val INPUT_FILE = """../../sampledata/small_access.log"""

class LogParser : ILogParser {


    internal val logEntries:MutableList<LogEntry> = ArrayList()

    override fun loadLogEntries(): Result<FileResult> {
        try {
            loadLogEntries(INPUT_FILE)
        } catch (e: java.io.FileNotFoundException) {
            return Result(error = true, errorMessage = "Could not load the log file: ${INPUT_FILE}")

        }
        return Result(data = FileResult(1, logEntries.size))
    }

    /**
     * Load the log entries from a file
     */
    override fun loadLogEntries( fileName: String ) : Result<FileResult>{
        File(fileName).reader().use {
            it.forEachLine {line ->

                logEntries.add(processLogEntry(line))
            }
        }
        return Result(data = FileResult(1, logEntries.size))
    }

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    internal fun processLogEntry( logLine: String): LogEntry {
        val tokens = tokeniseLine(logLine)
        var logEntry = LogEntry()
        if(tokens != null)
            logEntry = buildLogEntry(tokens)
        return logEntry
    }


    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
    internal fun getUniqueRequestIPs(logEntries: MutableList<LogEntry>): List<LogEntry>{
        return logEntries.distinctBy { it.ipAddr }
    }


    override fun filterLogsByUserAgent(): Result<List<UACount>> {
        val distinctEntries = logEntries.distinctBy { it.userAgent }
        println(">>>>>>>>>> Size of distinctEntries = ${distinctEntries.size}")
        val entriesByUA = ArrayList<UACount>()
        // Now count each one
        for(entry in distinctEntries){
            val count = logEntries.count { it.userAgent == entry.userAgent }
            entriesByUA.add(UACount(entry.userAgent, count))
        }
        entriesByUA.sortBy { it.count }
        entriesByUA.reverse()
        return Result(data = entriesByUA)
    }


    override fun filterLogsByUniqueRequestIP(): Result<List<ReqCount>> {
        var distinctEntries =getUniqueRequestIPs(logEntries)
        if(distinctEntries.size > 10000) {
            distinctEntries = distinctEntries.subList(0, 10000) // Temp -- truncate the list -- for speed of testing
        }
        val entriesByReqIp = ArrayList<ReqCount>()
        val elapsedTime = measureTimeMillis {
            for (entry in distinctEntries) {
                val count = logEntries.count { it.ipAsInt == entry.ipAsInt }
                entriesByReqIp.add(ReqCount(entry.ipAddr, count))
            }
        }
        println("Elapsed time: $elapsedTime (milliseconds)")
        entriesByReqIp.sortBy { it.count }
        entriesByReqIp.reverse()
        return Result(data = entriesByReqIp)

    }

} // End of the class
