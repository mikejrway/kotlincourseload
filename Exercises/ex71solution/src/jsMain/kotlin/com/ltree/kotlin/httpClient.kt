package com.ltree.kotlin

import org.w3c.xhr.XMLHttpRequest
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


suspend fun httpPutOrPost(url: String, data: String, httpVerb: HTTPMethod, contentType: String = "application/json; charset=utf-8"): String = suspendCoroutine { c ->
    when (httpVerb) {
        HTTPMethod.POST, HTTPMethod.PUT -> {
            val xhr = XMLHttpRequest()
            xhr.onreadystatechange = { _ -> readyStateHandler(xhr, c) }
            xhr.open(httpVerb.name, url, true)
            xhr.setRequestHeader("Content-type", contentType)
            xhr.send(data)
        }
        else -> console.log("An unsupported verb was passed through to this function")
    }
}

suspend fun httpGet(url: String): String = suspendCoroutine { c ->
    val xhr = XMLHttpRequest()
    xhr.onreadystatechange = { _ -> readyStateHandler(xhr, c) }
    xhr.open("GET", url)
    xhr.send()
}


fun readyStateHandler(xhr: XMLHttpRequest, coroutineContext: Continuation<String>) {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        if (xhr.status.toInt() == 200) {
            coroutineContext.resume(xhr.response as String)
        } else {
            var message = "Is the server running?"
            // Most likely cause of a zero status is a connection refused: server not running
            if(xhr.status.toInt() != 0){
                message = xhr.statusText
            }
            coroutineContext.resumeWithException(RuntimeException("HTTP error code: ${xhr.status}, message: $message"))
        }
    }
}


enum class HTTPMethod {
    POST,
    GET,
    PUT,
    UPDATE,
    DELETE
}


private fun xhrRequest(url: String, method: String = "GET", callback: (String) -> Unit) {
    val xmlHttp = XMLHttpRequest()
    xmlHttp.open(method, url)
    xmlHttp.onreadystatechange = {
        if(xmlHttp.readyState == XMLHttpRequest.DONE ){
            if(xmlHttp.status / 100 == 200){
                callback(xmlHttp.responseText)
            } else {
                console.log("onError() handler:  ${xmlHttp.status} : ${xmlHttp.statusText}")
            }
        }
    }

    xmlHttp.send()
}