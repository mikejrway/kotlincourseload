package com.ltree.kotlin

import kotlinx.coroutines.*
import kotlinx.html.*
import kotlinx.html.dom.append
import kotlinx.html.js.*
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLElement
import kotlin.browser.document

const val BUTTON_LOAD = "load"
const val BUTTON_BY_IP = "byIp"
const val BUTTON_BY_UA = "byUa"

//T Step 1: Add a main function: the point at which the JavaScript code execution starts
//T Step 2: In the main function call updateState(ViewState.INITIAL)
//T Step 3: In the main function call addButtonsHandlers() immediately before updateState(ViewState.INITIAL)
//-
fun main() {
    addButtonsHandlers()
    updateState(ViewState.INITIAL)
}
//+

private fun addButtonsHandlers() {

//T Step 4: Examine the code below
    (document.getElementById(BUTTON_LOAD) as HTMLButtonElement).onclick = {
        GlobalScope.launch {
            loadLogFiles_ButtonAction()
        }
    }

//T Step 5: Using the code above as a template add an onclick handler for the button with an id of BUTTON_BY_IP
//T The handler should call reportByIP_ButtonAction()
//-
    (document.getElementById(BUTTON_BY_IP) as HTMLButtonElement).onclick = {
        GlobalScope.launch {
            reportByIP_ButtonAction()
        }
    }
//+

//T Step 6: Using the code above as a template add an onclick handler for the button with an id of BUTTON_BY_UA
//T The handler should call reportByUser_ButtonAgentAction()
//-
    (document.getElementById(BUTTON_BY_UA) as HTMLButtonElement).onclick = {
        GlobalScope.launch {
            reportByUser_ButtonAgentAction()
        }
    }
//+

}

/**
 * onClick handler for the load log files button
 * Calls the httpGet function to make a request to the server for data
 * When the response is received it creates a table showing the results of the request
 * NB: This must be a suspending function
 */
private suspend fun loadLogFiles_ButtonAction() {

    updateState(ViewState.BUSY)  // Update the button states
    showError("")

    try {
//T Step 7: call the httpGet function with a URL of "http://localhost:8888/loadLogs" save the result in val response
//R     val response=""
//-
        val response = httpGet("http://localhost:8888/loadLogs")
//+
        console.log("LOG MESSAGE" + response)
        val result = JSON.parse<Result<FileResult>>(response)
        if (result.error == false && result.data != null) {

//T Step 8: Use  document.getElementById("results") to get a reference to the results DIV

//T Step 9: Use also and a lambda function to set the innerHTML of the element to ""
//H ?.also{ it.innerHTML="" }
//T Step 10: Start a lambda block with append
//T Step 11: In the block create a table element
//H There is a slide in the course notes with very similar code to that required here
//T Step 12: In the table element create a row with two th elements
//T Step 13: Set the text of the th elements to "Files Processed" and "Total Log Lines"
//T Step 14: Create another tr with two td elements
//T Step 15: Set the text of the first td to  result.data.fileCount and the second to result.data.logEntryCount
//H result.data is the data fetched from the server
//-
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
                ?.append {
                    table {
                        tr {
                            th { text("Files Processed") }
                            th { text("Total Log Lines") }
                        }
                        tr {
                            td { text(result.data.fileCount) }
                            td { text(result.data.logEntryCount) }
                        }
                    }
                }
//+
            updateState(ViewState.LOADED)   // Update the button states
        } else {
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
            showError(result.errorMessage)
            updateState(ViewState.INITIAL)  // Update the button states
        }
    } catch (e: RuntimeException) {
        showError("Something went wrong : ${e.message}")
    }
    //}
}

/**
 * Button click handler for the report by IP button
 */
private suspend fun reportByIP_ButtonAction() {
    try {
        showError("")
        updateState(ViewState.BUSY)
        val response = httpGet("http://localhost:8888/filterByIPFreq")
        console.log("LOG MESSAGE" + response)
        val result = JSON.parse<Result<Array<ReqCount>?>>(response)

        if (result.data != null) {
            val resultList: List<ReqCount> = result.data.toList()
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
                ?.append {
                    createReqIP_Table(resultList)
                }
        } else {
            showError("No entries returned")
        }
        updateState(ViewState.LOADED)
    } catch (e: RuntimeException) {
        showError("Something went wrong : ${e.message}")
    }

}

private fun TagConsumer<HTMLElement>.createReqIP_Table(resultList: List<ReqCount>) {
    table {
        tr {
            th { text("Requesting IP") }
            th { text("# Requests") }
        }

        for (reqCount in resultList) {
            tr {
                td { text(reqCount.ipAddr as String) }
                td { text(reqCount.count) }
            }
        }
    }
}


/**
 * Button click handler for the report by User Agent button
 */
private suspend fun reportByUser_ButtonAgentAction() {
    try {
        showError("")
        updateState(ViewState.BUSY)
        val response = httpGet("http://localhost:8888/filterByUA")
        console.log("LOG MESSAGE" + response)
        val result = JSON.parse<Result<Array<UACount>?>>(response)

        if (result.data != null) {
            val resultList: List<UACount> = result.data.toList()
            document.getElementById("results")
                ?.also { it.innerHTML = "" }
                ?.append {
                    createUA_Table(resultList)
                }
        } else {
            showError("No entries returned")
        }
        updateState(ViewState.LOADED)
    } catch (e: RuntimeException) {
        showError("Something went wrong : ${e.message}")
    }
}

/** Creates the table for the report by UA.
 * Uses the uacol and countcol classes to set the column widths
 */
private fun TagConsumer<HTMLElement>.createUA_Table(resultList: List<UACount>) {
    table {
        tr {
            th {
                classes += "uacol"
                text("User Agent")
            }
            th {
                classes += "countcol"
                text("# Requests")
            }
        }

        for (reqCount in resultList) {
            tr {
                td {
                    classes += "uacol"
                    text(reqCount.uaName as String)
                }
                td {
                    classes += "countcol"
                    text(reqCount.count)
                }
            }
        }
    }
}


/**
 * Implementation of a state-management pattern to control the enabling of buttons
 */
private fun updateState(state: ViewState) {
    when (state) {
        ViewState.INITIAL -> {
            disableButton(BUTTON_LOAD, false)
            disableButton(BUTTON_BY_IP, true)
            disableButton(BUTTON_BY_UA, true)
        }
        ViewState.LOADED -> {
            disableButton(BUTTON_LOAD, false)
            disableButton(BUTTON_BY_IP, false)
            disableButton(BUTTON_BY_UA, false)
        }
        ViewState.BUSY -> {
            disableButton(BUTTON_LOAD, true)
            disableButton(BUTTON_BY_IP, true)
            disableButton(BUTTON_BY_UA, true)
        }
    }

}

private fun disableButton(id: String, state: Boolean) {

    val but : HTMLButtonElement? = document.getElementById(id) as HTMLButtonElement
    console.log("Updating button: $id")
    but?.disabled = state

}


private fun showError(error: String) {
    document.getElementById("errors")
        ?.also { it.innerHTML = "" }
        ?.append {
            h1 {
                text(error)
            }
        }
}

/**
 * UI states
 */
private enum class ViewState {
    INITIAL, LOADED, BUSY
}

