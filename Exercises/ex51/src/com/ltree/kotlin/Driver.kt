package com.ltree.kotlin

import mu.KotlinLogging


// TODO - Bonus Step 1: Note that we have initialised a logger for you
val logger = KotlinLogging.logger {}

fun main(){

    // TODO Bonus Step 2: Add exception handler (try / catch) around the call to processLogEntries
        LogParser().processLogEntries()
// TODO - Bonus Step 3: In the catch block print a message to the user
// *HINT* Perhaps : "A serious error has occurred. See the log file for details"
// TODO - Bonus Step 4: Use the logger to output a message
// *HINT* logger.error takes the exception and a lambda function (the last statement is the return value which then gets logged)
}
