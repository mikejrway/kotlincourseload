package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
import mu.KotlinLogging
import java.io.File

const val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry2="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry3="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry4="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry5="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry6="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry7="""95.29.198.15 - - [12/Dec/2015:18:32:10 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""


// TODO Step 1: Add a const INPUT_FILE initialised to """../../sampledata/small_access.log"""


class LogParser : ILogParser {

// TODO Step 9: Remove the initialisation of the array with test data then initialise the array to a length of 10_000 with an empty string in each element
// *HINT* Array(10_000){""}
// *HINT* This is inelegant and we will fix it in the next exercise
 internal val logFileLines = arrayOf(entry1, entry2,entry3,entry4,entry5,entry6,entry7 )

    private val logEntries: Array<LogEntry> = Array(logFileLines.size){ LogEntry() }


    override fun processLogEntries(){
// TODO Step 10: Call loadLogEntries passing INPUT_FILE as the parameter

        var index = 0
        for(logLine in logFileLines){
            logEntries[index++] = processLogEntry(logLine)
        }


        val filteredLogEntries = getUniqueRequestIPs(logEntries)
        filteredLogEntries.forEach { println(it)}

        println("Processing Complete ${logFileLines.size} entries processed")
    }

    /**
     * Load the log entries from a file
     */
// TODO Step 2: Create a new function  called loadLogEntries() parameter is a fileName type String. Return type is Unit

// TODO Step 3: Create a variable called index with a value of 0

// TODO Step 3: Create a File object from the fileName

// TODO Step 4: Create a reader from the File object

// TODO Step 5: Create a use block on the reader


// TODO Step 6: Use the forEachLine function of the reader to iterate through each line in the file
// *HINT* The reader is it inside the lambda

// TODO Step 7: Add each line of the file to logFileLines
// *HINT* Use the index variable to index into the array

// TODO Step 8: Increment the value of index

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    internal fun processLogEntry( logLine: String): LogEntry {
        val ipAddress = parseIPAddress(logLine)
        val url = parseRequestURL(logLine)
        return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
    }

    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */

    internal fun getUniqueRequestIPs(logEntries: Array<LogEntry>): Array<LogEntry>{
        return logEntries.distinctBy { it.ipAddr }.toTypedArray()
    }

} // End of the class

