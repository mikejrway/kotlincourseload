package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
import mu.KotlinLogging
import java.io.File


// TODO Step 1: Delete these test entries (entry1 - entry7) as they are no longer needed

const val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry2="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry3="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry4="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry5="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry6="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry7="""95.29.198.15 - - [12/Dec/2015:18:32:10 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""

const val INPUT_FILE = """../../sampledata/small_access.log"""

class LogParser : ILogParser {

    // TODO Step 2: Delete the logFileLines array as we no longer need it
     internal val logFileLines = Array(1000){""}


    // TODO Step 3: Convert logEntries to MutableList<LogEntry> initialise with ArrayList<String>
     private val logEntries: Array<LogEntry> = Array(logFileLines.size){ LogEntry() }

    override fun processLogEntries(){

        loadLogEntries(INPUT_FILE)

    // TODO Step 4: Note that we have removed the redundant code which iterated through the log lines and populated logEntries
/*
        var index = 0
        for(logLine in logFileLines){
            logEntries[index++] = processLogEntry(logLine)
        }
*/
        val filteredLogEntries = getUniqueRequestIPs(logEntries)
        filteredLogEntries.forEach { println(it)}


        println("Processing Complete ${logEntries.size} entries processed")
    }

    /**
     * Load the log entries as objects from a file
     */
    internal fun loadLogEntries( fileName: String ){
    // TODO Step 5: Delete the index variable as it is no longer required
         var index = 0
        File(fileName).reader().use {
    // TODO Step 6: Add an explicit parameter to the lambda declaration (named logLine)
         it.forEachLine {

// TODO Step 7: Modify the code below so that it passes logLine as an argument to the processLogEntry() function
// TODO Step 8: Add the return of processLogEntry() to the logEntries collection

                 logFileLines[index++]=it
            }
        }
    }

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    internal fun processLogEntry( logLine: String): LogEntry {
        val ipAddress = parseIPAddress(logLine)
        val url = parseRequestURL(logLine)
        return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
    }

    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
// TODO Step 9: Change the argument type to MutableList<LogEntry> and the return types to List<LogEntry>
 internal fun getUniqueRequestIPs(logEntries: Array<LogEntry>): Array<LogEntry>{
// TODO Step 10: Remove the call to toTypedArray (as we are now returning a List)
        return logEntries.distinctBy { it.ipAddr }.toTypedArray()
    }

} // End of the class

