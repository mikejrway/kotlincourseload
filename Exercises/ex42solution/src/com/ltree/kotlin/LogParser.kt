package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL

const val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry2="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry3="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry4="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry5="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry6="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry7="""95.29.198.15 - - [12/Dec/2015:18:32:10 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""


//T Step 1: Create a class called LogParser wrap all the code in the file inside the class brackets
//-
class LogParser : ILogParser {
//-
// No TD statement needed here.
    private val logFileLines = arrayOf(entry1, entry2,entry3,entry4,entry5,entry6,entry7 )

    private val logEntries: Array<LogEntry> = Array(logFileLines.size){ LogEntry() }
//+


//+
//T Step 2: rename the main function to processLogEntries
//R fun main(){
//-
override fun processLogEntries(){
//+

//T Step 3:  Move these two array declarations  to be fields of the class and mark them private
//H They do not need to be fields of the class yet but things will change in subsequent exercises
//H They need to be outside of the function but inside the class

//R    val logFileLines = arrayOf(entry1, entry2,entry3,entry4,entry5,entry6,entry7 )
//-
//+
//R    val logEntries: Array<LogEntry> = Array(logFileLines.size){ LogEntry() }
//-
//+

    var index = 0
    for(logLine in logFileLines){
        logEntries[index++] = processLogEntry(logLine)
    }


    val filteredLogEntries = getUniqueRequestIPs(logEntries)
    filteredLogEntries.forEach { println(it)}

    println("Processing Complete 1")
}


    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    //T Step 4: Make this function internal (a good way of reducing visibility but still making the code available to test)
//X+ :::internal:::
    internal fun processLogEntry( logLine: String): LogEntry {
//X-
        val ipAddress = parseIPAddress(logLine)
        val url = parseRequestURL(logLine)
        return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
    }

    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
    //T Step 5: Make this function internal
//X+ :::internal:::
    internal fun getUniqueRequestIPs(logEntries: Array<LogEntry>): Array<LogEntry>{
//X-
        val distinctLogEntries =  logEntries.distinctBy { it.ipAddr }
        return distinctLogEntries.toTypedArray()
    }
//-
} // End of the class
//+
