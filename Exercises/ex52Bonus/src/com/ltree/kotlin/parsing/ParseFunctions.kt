package com.ltree.kotlin.parsing

import com.ltree.kotlin.model.LogEntry
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

const val NO_IP = "No IP in entry"

val LOG_FILE_LINE_REGEX : Regex =  """([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}) \S* \S* \[(.*?)\] "(.*?)" (\d+) (\S+) "(.*?)" "(.*?)" "-"""".toRegex()
val dateFormat = DateTimeFormatter.ofPattern("dd/MMM/yyyy:kk:mm:ss XXXX")

/**
 * Constants identifying the groups from the regular expression
 */
const val GRP_IP = 1
const val GRP_TIMESTAMP = 2
const val GRP_URL = 3
const val GRP_RESPONSE_CODE=4
const val GRP_RESPONSE_SIZE=5
const val GRP_REFERRER = 6
const val GRP_USER_AGENT=7

/**
 * Parses a log entry string and extracts the IP address from it
 */
fun parseIPAddress( logLine: String): CharSequence {
    // Create a variable called ipAddress of type CharSequence and initialise it with NO_IP

    var ipAddress: CharSequence = NO_IP

    // Verify that the logLine has a length > 0 and that it does contain a space (indexOf(' ')).
    // If either condition is not true then return NO_IP

    ipAddress = NO_IP
    if(logLine.isNotEmpty()) {
        val firstSpace = logLine.indexOf(' ')
        ipAddress = logLine.subSequence(0, firstSpace)
    }
    return ipAddress
}

    // PART 2 Starts Here >>>

    // Create a new function parseRequestURL which will satisfy the requirements of the testParseRequestURL() test
    // The argument to the function should be a nullable String, the function should return a nullable CharSequence

fun parseRequestURL(logLine: String?): CharSequence? {

    // Define a variable: res of type CharSequence - it should be nullable.
    // Initialise the variable to null

    var res: CharSequence? = null

    // Use an If test to check that the parameter to this function is not null and not empty ...

    if(logLine != null && logLine.isNotEmpty()) {


    // Use indexOf() to find the position of  the first and second double quote (") in the logLine
        val firstQuote = logLine.indexOf('"')
        val secondQuote = logLine.indexOf('"', firstQuote + 1)
    // Populate the res variable with the text between the two quotes

        res = logLine.subSequence(firstQuote + 1, secondQuote)

    }

    return res
}

fun ipAddrToInt(addr: CharSequence) : Int {
    var parts = addr.split('.').map{it.toInt()};

    return (parts[0] shl 24) + (parts[1] shl 16) + (parts[2] shl 8) + parts[3]
};

fun buildLogEntry(logFields: List<String>): LogEntry {
    val dateTime = parseTimeStamp(logFields[GRP_TIMESTAMP]).toLocalDateTime()
    var responseCode = -1
    if(logFields[GRP_RESPONSE_CODE] != "-") {
        responseCode = logFields[GRP_RESPONSE_CODE].toInt() // May need to modify C2 to include this conversion
    }
    // No risk of an empty String as the source method sets non-matched groups to ""
    val logEntry = LogEntry(
        ipAddr = logFields[GRP_IP],
        dateTime = dateTime,
        requestURL = logFields[GRP_URL],
        responseCode = responseCode,
        userAgent = logFields[GRP_USER_AGENT],
        referrer = logFields[GRP_REFERRER]
    )
    return logEntry
}


fun parseTimeStamp(timestamp: String): ZonedDateTime {
    return ZonedDateTime.parse(timestamp, dateFormat)
}

fun tokeniseLine(line: String) : List<String>? {


    val matchResults = LOG_FILE_LINE_REGEX.find(line)
    return matchResults?.groupValues
}
