package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.buildLogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
import com.ltree.kotlin.parsing.tokeniseLine
import mu.KotlinLogging
import java.io.File
import kotlin.system.measureTimeMillis


const val INPUT_FILE = """../../sampledata/small_access.log"""

// TODO -- Add a data class called UACount with two properties: val uaName: CharSequence, val count:Int
// TODO -- Add a data class called ReqCount with two properties: val ipAddr: CharSequence, val count:Int
// *HINT* It's probably more logical to put these two helper classes at the end of this file or in another file. We've put them here so you do them first!

class LogParser : ILogParser {


    internal val logEntries:MutableList<LogEntry> = ArrayList()


    override fun processLogEntries(){

        loadLogEntries(INPUT_FILE)

// TODO Optionally comment out these two lines to keep the display simple

        val filteredLogEntries = getUniqueRequestIPs(logEntries)
        filteredLogEntries.forEach { println(it)}

        println("Loading Complete ${logEntries.size} entries processed")

// TODO When you have finished the reportLogsByUniqueRequestIP() function, return here and add code to print out the report entries
// *HINT* You can use the code above as a template

        //val filteredLogEntries = reportLogsByUniqueRequestIP()

    }

    /**
     * Load the log entries as objects from a file
     */
    internal fun loadLogEntries( fileName: String ){

        File(fileName).reader().use {
            it.forEachLine {logLine ->
                    logEntries.add(processLogEntry(logLine))
            }
        }
    }

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    // TODOOOD -- Note that we have enhanced this function
    internal fun processLogEntry( logLine: String): LogEntry {
        val tokens = tokeniseLine(logLine)
        var logEntry = LogEntry()
        if(tokens != null)
            logEntry = buildLogEntry(tokens)
        return logEntry
    }


    /**
     * Returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */

    internal fun getUniqueRequestIPs(logEntries: MutableList<LogEntry>): List<LogEntry>{
        return logEntries.distinctBy { it.ipAddr }
    }


// TODO Add a function reportLogsByUniqueRequestIP() which returns List<ReqCount>
// TODO In this function you will count the number of requests from each unique IP address and return a collection of IP addresses and counts

// TODO In the function call getUniqueRequestIPs(logEntries) and assign the result to val distinctEntries

// TODO Create a new variable entriesByReqIp of type MutableList<ReqCount>

// TODO For each entry in  distinctEntries count the number of  entries in logEntries matching the ip address of the  distinctEntry then...
// TODO Create a new ReqCount with the distinct IP address and the count. Add the new object to entriesByReqIP
// *HINT* We have added a property to the LogEntry class called ipAsInt which is an integer version of the IP address

// TODO Sort entriesByReqIp by the count

// TODO Reverse the order of entriesByReqIp

// TODO Return entriesByReqIp


// TODO  BONUS2: Create a new function: fun reportLogsByUserAgent(): List<UACount>
// TODO BONUS2: In the function you need to do the following:
/*
    Create a distinctList of logEntries filtered by userAgent
    Use the map function of the distinctList to transform the list into a list of UACount
    Each entry in the UACount list should hold the name of the user agent and the number of requests from that UA
 */
// TODO BONUS2: Once the function is complete return to  processLogEntries  and add the code to print the list
} // End of the class

