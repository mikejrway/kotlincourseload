package com.ltree.kotlin

import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
import khttp.get
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import mu.KotlinLogging
import java.io.File
import java.io.StringReader
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.concurrent.TimeUnit
import kotlin.system.measureNanoTime
import kotlin.system.measureTimeMillis

const val INPUT_FILE = """../../sampledata/small_access.log"""
const val SERVICE_URL = "http://localhost:9090/logserver/"

class LogParser : ILogParser {

    internal val logFileLines: MutableList<String> = ArrayList()

    private val logEntries:MutableList<LogEntry> = ArrayList()

// TODO Step 10: Create a Channel of type LogEntry assign it to val channel


    override fun processLogEntries() {
        println("Loading Log Files")
// TODO Step 11: Create a GlobalScope.launch block save the job reference in a val receiverJob
// TODO Step 12: Inside the launch block: call channelReceiver() -- you will write this function soon
        var elapsedTime = 0L

        elapsedTime = measureTimeMillis {

// TODO Step 3: Add runBlocking blocking block enclosing the for loop

// TODO Step 1: Enclose the call to loadLogEntriesFromServer with a for loop iterating the variable i from 0..10. T

// TODO Step 2: Replace the fileName argument with "split$i.log"

// TODO Step 6: Surround loadLogEntriesFromServer() (just the code inside the for loop) with a launch block

                        loadLogEntriesFromServer(INPUT_FILE)
        } // End measure time block

        // Execution only gets to here once all the coroutines launched in runBlocking have completed
// TODO Step 13: Close the channel
// TODO Step 14: Create a runBlocking block. Inside the block call receiverJob.join() to wait for the channel receiver to finish

        val filteredLogEntries = getUniqueRequestIPs(logEntries)
        filteredLogEntries.forEach { println(it) }

        println("Processing Complete ${logEntries.size} entries processed.")
        val seconds = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.MILLISECONDS);
        println("Elapsed time: $seconds (seconds) $elapsedTime (milliseconds)")
    }

    // TODO Step 15: Create a new suspending function called channelReceiver()
    // TODO Step 16: Inside the function call channel.consumeEach
    // *HINT* The channel contains LogEntry objects
    // TODO Step 17: In the lambda block add the new LogEntry to logEntries

    /**
     * Load log files from an HTTP server
     */
// TODO Step 4: Add suspend modifier
     private fun loadLogEntriesFromServer( fileName: String ){

        val paramMap = mapOf("fileName" to fileName)
// TODO Step 5: Surround the remaining code in this function with a withContext(Dispatchers.IO) block
            val response = get(SERVICE_URL, params = paramMap)
            val reader = StringReader(response.text)

            reader.use {
                    reader ->
                reader.forEachLine {
                        line ->
// TODO Step 18: Wrap the line below in a launch {} block
// TODO Step 19: Modify the code in the launch block to send the LogEntry to the channel instead of adding it to logEntries
// *HINT* Objects are sent to a channel
                 logEntries.add(processLogEntry(line))
                }
            }
    }

    /**
     * Load the log entries from a file
     */
    internal fun loadLogEntries( fileName: String ){
        File(fileName).reader().use {
            it.forEachLine {
                logFileLines.add(it)
            }
        }
    }

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    internal fun processLogEntry( logLine: String): LogEntry {
        val ipAddress = parseIPAddress(logLine)
        val url = parseRequestURL(logLine)
        return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
    }

    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
    internal fun getUniqueRequestIPs(logEntries: MutableList<LogEntry>): List<LogEntry>{
        return logEntries.distinctBy { it.ipAddr }
    }

} // End of the class

