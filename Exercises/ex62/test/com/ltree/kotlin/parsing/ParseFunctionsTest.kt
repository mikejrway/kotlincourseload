package com.ltree.kotlin.parsing

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ParseFunctionsTest {

    private val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""

    @BeforeEach
    fun init() {

    }

    @Test
    fun testParseIPAddress() {
        val res = parseIPAddress(entry1)
        assertEquals("109.169.248.247", res)
    }

    @Test
    fun testParseIPAddress_EmptyString() {
        val res = parseIPAddress("")
        assertEquals(NO_IP, res)

    }


    @Test
    fun testParseRequestURL(){
        val res = parseRequestURL(entry1)
        assertEquals("GET /administrator/ HTTP/1.1", res)
    }

    @Test
    fun testParseRequestURL_EmptyString(){

        val res = parseRequestURL("")
        assertEquals(null, res)

    }


}
