package com.ltree.kotlin


import com.ltree.kotlin.model.LogEntry
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class LogParserTest {

    private val entry1 =
        """109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""

    @BeforeEach
    fun init() {

    }

    @Test
    fun testProcessLogEntry() {
        val expected = LogEntry(ipAddr="109.169.248.247", requestURL="GET /administrator/ HTTP/1.1")
        val res = processLogEntry(entry1)
        assertEquals(expected, res) 
    }

//T When you have written the code for getUniqueRequestIPs(): remove the comments around this test
//C+
    @Test
    fun testGetUniqueRequestIPs(){
        val entry1 = LogEntry(ipAddr="109.169.248.247", requestURL="GET /administrator/ HTTP/1.1")
        val entry2 = LogEntry(ipAddr="109.169.248.248", requestURL="GET /administrator/ HTTP/1.1")
        val entry3 = LogEntry(ipAddr="109.169.248.247", requestURL="GET /administrator/ HTTP/1.1")

        val entries = arrayOf(entry1, entry2, entry3)

        val res = getUniqueRequestIPs(entries)
        assertEquals(2, res.size, "There should be only two entries in the array")
    }
//C-
}