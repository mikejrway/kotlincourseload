package com.ltree.kotlin
//-
import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.parseIPAddress
import com.ltree.kotlin.parsing.parseRequestURL
//+

const val entry1="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry2="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry3="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry4="""46.72.177.4 - - [12/Dec/2015:18:31:08 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry5="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry6="""83.167.113.100 - - [12/Dec/2015:18:31:25 +0100] "POST /administrator/index.php HTTP/1.1" 200 4494 "http://almhuette-raith.at/administrator/" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
const val entry7="""95.29.198.15 - - [12/Dec/2015:18:32:10 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""


fun main(){
    // Create an array called logFileLines from the constants above
    val logFileLines = arrayOf(entry1, entry2,entry3,entry4,entry5,entry6,entry7 )

//T Step 1: Create a variable called logEntries of type Array<LogEntry> initialise the array to be the same size
// as the logFileLines array
//H How do you find the size of an array?
//T Step 2: To initialise the array you must also provide a lambda function which returns data with which to populate the array
//T Step 3: In this case: provide a function which returns a LogEntry object created with the no-argument constructor
//-
    val logEntries: Array<LogEntry> = Array(logFileLines.size){ LogEntry() }
//+


//T Step 4: Create a new variable called index and initialise it to 0
//-
    var index = 0
//+
    for(logLine in logFileLines){
//T Step 5: Modify the code below to assign the result of calling processLogEntry() to the logEntries array at a position defined by index

//R         processLogEntry(logLine)
//-
        logEntries[index++] = processLogEntry(logLine)
//+
//T Step 6: Increment the value of index

    }

//T <<< Stop here and return to the exercise manual

//T Step 16: Call getUniqueRequestIPs() passing the logEntries array as the parameter
//T Step 17: Assign the result to a new variable filteredLogEntries
//-
    val filteredLogEntries = getUniqueRequestIPs(logEntries)
//+
//T Step 18: Print out the new array
//H You could use the Array.forEach() method and provide a lambda that prints 'it'
//-
    filteredLogEntries.forEach { println(it)}
//+

    println("Processing Complete 1")
}


/**
 * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
 */
//T Step 7: Modify the function to return a LogEntry
//R fun processLogEntry( logLine: String){
//-
fun processLogEntry( logLine: String): LogEntry {
//+

//T Step 8: Remove the println and assign the result to a new variable called ipAddress
//H var or val ?
//-
    val ipAddress = parseIPAddress(logLine)
//+

//T Step 9: Remove the println and assign the result to a new variable called url
//H var or val ?
//-
    val url = parseRequestURL(logLine)
//+

//T Step 6: Create a new logEntry passing just ipAddress and url as the arguments. Assign to a new variable logEntry
//H You will need to use named function parameters
//H url may be null so you will have to handle this.
//H You can: wrap the call with an if statement, use the !! operator on url (which throws a null pointer exception)
//H Or you could use the Elvis operator (from the previous chapter) which allows you to supply a default value

//T Step 10: Return the logEntry
//-
    return LogEntry(ipAddr = ipAddress, requestURL = url ?: "")
//+

}

/**
 * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
 */
//T Step 11: Create a function called getUniqueRequestIPs
//T Step 12: The function should expect a paramter called logEntries of type Array<LogEntry>
//T Step 13: it should return Array<LogEntry>
//-
fun getUniqueRequestIPs(logEntries: Array<LogEntry>): Array<LogEntry>{
//+

//T Step 14: Call the distinctBy function on logEntries store the result in a new val: distinctLogEntries.
// It expects an argument which is a lambda function
//T Step 15: Provide a lambda function which returns the ipAddr property of each LogEntry

//H As there is a single argument passed to the lambda you can use 'it' to access it
//H The actual code you need for this step is in the exercise manual
//-
    val distinctLogEntries =  logEntries.distinctBy { it.ipAddr }
//+

//T Step 16: Convert distinctLogEntries to an array by calling toTypedArray(). Return the resultant array
//-
    return distinctLogEntries.toTypedArray()
}
//+