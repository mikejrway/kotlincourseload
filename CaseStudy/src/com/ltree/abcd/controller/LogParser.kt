package com.ltree.abcd.controller

import com.ltree.abcd.model.HttpStatusCode
import com.ltree.abcd.model.LogEntry
import com.ltree.abcd.model.parseTimeStamp
import com.sun.org.apache.xpath.internal.operations.Bool
import khttp.get
import kotlinx.coroutines.runBlocking
import java.io.File
import java.io.StringReader
import java.time.LocalDateTime
import kotlin.collections.ArrayList

const val INPUT_FILE = """sampledata/small_access.log"""
//const val INPUT_FILE = """sampledata/access.log"""
//                                      IP                                                    Timestamp req     resp  length     ua
val LOG_FILE_LINE_REGEX : Regex =  """([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}) \S* \S* \[(.*?)\] "(.*?)" (\d+) (\S+) "(.*?)" "(.*?)" "-"""".toRegex()

/**
 * Constants identifying the groups from the regular expression
 */
val GRP_IP = 1
val GRP_TIMESTAMP = 2
val GRP_URL = 3
val GRP_RESPONSE_CODE=4
val GRP_RESPONSE_SIZE=5
val GRP_REFERRER = 6
val GRP_USER_AGENT=7
//val GRP_RESPONSE=7


fun main(){
/* File based solution
    // Open the file
    //val reader = File(INPUT_FILE).reader()
*/

// HTTP based solution !
    val params = mapOf("fileName" to "small_access.log")
    val r = get("http://localhost:8080/logserver/", params=params)
    val reader = StringReader(r.text)
// End of Http changes!!
    var lineCount = 0
    var logEntries = ArrayList<LogEntry>()
    val splitLogEntries = ArrayList<List<LogEntry>>()
    var splitCount = 1000
    val splitIndex = 0



        // Read each line in turn
    println("Starting: ${LocalDateTime.now()}")
        reader.forEachLine {
            lineCount++
            // Tokenise each line
            if(it.length > 0) {
                val logFields = tokeniseLine(it)


                // Create objects from the tokens
                if (logFields != null && logFields?.size > 0) {
                    val logEntry = buildLogEntry(logFields)
                    logEntries.add(logEntry)
                    if(--splitCount == 0){
                         // Once we have collected 1000 entries: add them to the splitLogEntries list
                        // and start again
                        splitLogEntries.add(logEntries)
                        logEntries=ArrayList()
                        splitCount=1000
                    }
                } else {
                    println("ERROR: $it")
                }
            }
        }

    /**
     * At this stage: splitLogEntries contains multiple lists of log entries.
     * The plan is now to process each one as a coroutine
     * Then by some magic merge the lot!
     */
    println( "$lineCount lines processed")
    println("File processing Ending: ${LocalDateTime.now()}")
    println("Size of split log array: ${splitLogEntries.count()}")
    println("Num elements in log file: ${logEntries.size}")
    // close the file
    reader.close()

    // Exceptions?

    val distinctLogEntries: List<LogEntry> = getUniqueRequestIPs(logEntries)
    println("Num unique IPs: ${distinctLogEntries.size}")
    println("=============================================")
    //sortIPsByNumberRequests(logEntries)
    /// Experiment: find those with error response codes (4XX or 5xx)
    println("Before filter = ${logEntries.size}")
    var filteredLogEntries = findFailedRequests(logEntries)
    println("After filter = ${filteredLogEntries.size}")
    /// Think this is really grouping not sorting!
    sortIPsByNumberRequests(filteredLogEntries)
    ////launchCoroutines(splitLogEntries)
    println("All Processing Ended: ${LocalDateTime.now()}")
}

/**
 * Creates a LogEntry from a line of the log file
 */
fun buildLogEntry(logFields: List<String>): LogEntry {
    val dateTime = parseTimeStamp(logFields[GRP_TIMESTAMP]).toLocalDateTime()
    var responseCode = -1
    if(logFields[GRP_RESPONSE_CODE] != "-") {
        responseCode = logFields[GRP_RESPONSE_CODE].toInt() // May need to modify C2 to include this conversion
    }
    // No risk of an empty String as the source method sets non-matched groups to ""
    val logEntry = LogEntry(
        ipAddr = logFields[GRP_IP],
        dateTime = dateTime,
        requestURL = logFields[GRP_URL],
        responseCode = responseCode,
        userAgent = logFields[GRP_USER_AGENT],
        referrer = logFields[GRP_REFERRER]
    )
    return logEntry
}


fun launchCoroutines(splitLogEntries: ArrayList<List<LogEntry>>){

    runBlocking{
        for(logEntries in splitLogEntries){
            sortIPsByNumberRequests(logEntries ) // Do something with the results here!
        }
    }

}

/**
 * Tokenise a line of text read from the log file
 * Note the return type may be null - we have to check for it somewhere!
 */

fun tokeniseLine(line: String) : List<String>? {


    val matchResults = LOG_FILE_LINE_REGEX.find(line)
    return matchResults?.groupValues
}




/**
 * Returns a List<LogEntry> where each entry has a dstinct (unique) IP address
 */
fun getUniqueRequestIPs(logEntries: List<LogEntry>): List<LogEntry>{
    // Using distinct gives us a list of unique IP's (changing the lambda would of course change what we return)
    return logEntries.distinctBy { it.ipAddr }
}

fun findFailedRequests(logEntries: List<LogEntry>) : List<LogEntry>{
    //return filterList(logEntries){it.responseCode > HttpStatusCode.CLIENT_ERR.start}
    return filterList(logEntries){HttpStatusCode.CLIENT_ERR.isStatusCodeInRange(it.responseCode) ||
                                HttpStatusCode.SERVER_ERR.isStatusCodeInRange(it.responseCode)}
}

/**
 * Filter function - although we are delegating to the List.filter function I've wrapped it so we can see how to
 * build a function that takes a Lambda
 */
fun filterList(logEntries: List<LogEntry>, filter: (LogEntry) -> Boolean) : List<LogEntry>{
    return logEntries.filter(filter)
}

fun sortIPsByNumberRequests(logEntries: List<LogEntry>) : List<List<LogEntry>>{
    // Get the distinct IP's
    val logsByDistinctIp = getUniqueRequestIPs(logEntries)
    // Now filter the list by those IP's -- getting a count of each one

    val listOfRequestsForEachIP = ArrayList<List<LogEntry>>() // An array of List<LogEntry>

    // First: create a list of entries for each IP
    for(logEntry in logsByDistinctIp){
        listOfRequestsForEachIP.add(logEntries.filter { it.ipAddr == logEntry.ipAddr })
    }
    // Then use count to count the number of entries

    val sortedList = listOfRequestsForEachIP.sortedBy { it.count() }

    for(requestList in sortedList){
        println( "${requestList[0].ipAddr} Request count= ${requestList.count()}")
    }

    return sortedList

}

fun chopFile(){
    // Read
}