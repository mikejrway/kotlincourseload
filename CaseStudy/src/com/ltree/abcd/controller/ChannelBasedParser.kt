package com.ltree.abcd.controller

import com.ltree.abcd.model.LogEntry
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import java.io.File

class ChannelBasedParser {

    private val channel = Channel<LogEntry>()
    public val logEntries = ArrayList<LogEntry>()

    suspend fun parseLogFile() {


        val reader = File(INPUT_FILE).bufferedReader()//.reader()
        log("Start parse")
        var line = reader.readLine()
        while(line != null){
            var x = line.subSequence(0,10)
            //println("Reading $x")
            // Insight: forEachLine is a function
            // Thus the launch must be within it or Kotlin does not see the call to send as being in a coroutine!

                val logFields = tokeniseLine(line)
                if (logFields != null && logFields?.size > 0) {
                    val logEntry = buildLogEntry(logFields)
                        /* emit log entries via a channel as they are discovered */
                    log("Sending ${logEntry.ipAddr}")
                    channel.send(logEntry)
                }
                line = reader.readLine()
            }

       channel.close()
       log("End parse")
    }


    /**
     *
     */
    suspend fun processLogEntries( id: Int){
        log("Start populate")

        for(logEntry in channel) {
            // Currently just adding the entries to a collection
            // Could do more interesting stuff at this point
            log("[$id]: Processing ${logEntry.ipAddr}")
            logEntries.add(logEntry)
        }
        log("End populate")
    }
}


fun CoroutineScope.parseLogFileProducer() : ReceiveChannel<LogEntry> = produce {

    val reader = File(INPUT_FILE).bufferedReader()//.reader()
    log("Start parse")
    var line = reader.readLine()
    while(line != null){
        // parse file
        val logFields = tokeniseLine(line)
        if (logFields != null && logFields?.size > 0) {
            val logEntry = buildLogEntry(logFields)
            /* emit log entries via a channel as they are discovered */
            log("Sending ${logEntry.ipAddr}")
            send(logEntry)
        }
        line = reader.readLine()

    }
    log("End parse")
}

/**
 * Works nicely using the producer builder to create the producing coroutine
 */
fun main2() = runBlocking{
    launch {
        val parser = ChannelBasedParser()
        // Start parsing the file
        val channel = parseLogFileProducer()
        // Now do some processing on the channel data
        log("Processing channel")
        channel.consumeEach {
            log("Adding ${it.ipAddr}")
            parser.logEntries.add(it)
        }
    }
    //p1.await()
    log("File parsing done")
    //p2.await()
    log("All done")
}

/**
 * This approach now works.
 * (
 */
fun main() = runBlocking{
        val parser = ChannelBasedParser()
        // Start parsing the file -- must be done async so that the parser can start too
        async {parser.parseLogFile()}
        //parser.parseLogFile()
        // Now do some processing on the channel data
        // In this example, we are launching 5 seperate coroutines to do the processing
        // An example of Fan-out

        repeat(5) {
            launch{parser.processLogEntries(it)}
        }


    //p1.await()
    println("File parsing done")
    //p2.await()
    println("All done")
}

var logId = 1;
fun log(msg: String) = println("[${logId++}][${Thread.currentThread().name}] $msg")