package com.ltree.abcd.model

import java.time.LocalDateTime
import java.time.Month
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
//12/Dec/2015:18:25:11 +0100
val dateFormat = DateTimeFormatter.ofPattern("dd/MMM/yyyy:kk:mm:ss XXXX")

data class LogEntry( val ipAddr: String, val dateTime: LocalDateTime, val requestURL: String, val responseCode: Int, val userAgent: String, val referrer: String ) {
    constructor(): this("", LocalDateTime.of(2020, Month.APRIL, 1, 1,1), "", 0 ,"","")
}

fun parseTimeStamp(timestamp: String): ZonedDateTime{
    return ZonedDateTime.parse(timestamp, dateFormat)
}