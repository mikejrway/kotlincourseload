package com.ltree.abcd.model

enum class HttpStatusCode(val start: Int, val end: Int) {
    SUCCESS(200, 299), REDIRECTION(300,399), CLIENT_ERR(400,499), SERVER_ERR(500, 599);

    fun isStatusCodeInRange(statusCode: Int): Boolean {
        return statusCode in start..end //  return statusCode >= start && statusCode <= end
    }
}