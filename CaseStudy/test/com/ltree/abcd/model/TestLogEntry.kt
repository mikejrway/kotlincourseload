package com.ltree.abcd.model


import com.ltree.abcd.controller.tokeniseLine
import kotlin.test.assertEquals
import org.junit.jupiter.api.Test
import java.time.ZoneId


class TestLogEntry {

    /*
    Make sure we are correctly parsing the timestamp
     */
    @Test fun testParseTimeStamp() {

        val testLine = "10/Dec/2015:18:25:11 +0100"
        val dateTime = parseTimeStamp(testLine)


        assertEquals(2015, dateTime.year, "Wrong year!")
        assertEquals(10,  dateTime.dayOfMonth, "Wrong day of month!")
        assertEquals(12, dateTime.monthValue, "Wrong month!")
        assertEquals(18, dateTime.hour, "Wrong hour!")
        assertEquals(25, dateTime.minute, "Wrong minute!")
        assertEquals(11, dateTime.second, "Wrong second!")
        assertEquals(ZoneId.of("+0100"), dateTime.zone, "Wrong timezone!")


    }

}