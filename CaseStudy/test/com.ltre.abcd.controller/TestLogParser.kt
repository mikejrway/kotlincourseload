package com.ltre.abcd.controller


import com.ltree.abcd.controller.getUniqueRequestIPs
import com.ltree.abcd.controller.tokeniseLine
import com.ltree.abcd.model.LogEntry
import org.junit.jupiter.api.BeforeEach
import kotlin.test.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDateTime


class TestLogParser {

    private val logEntry1 = LogEntry("109.169.248.247", LocalDateTime.now(), "requestURL1",200, "userAgent1", "referrer1" )
    private val logEntry1a = LogEntry("109.169.248.247", LocalDateTime.now(), "requestURL1a", 200, "userAgent1a", "referrer1a" )
    private val logEntry2 = LogEntry("109.169.248.248", LocalDateTime.now(), "requestURL2", 200, "userAgent2", "referrer2" )
    private val logEntry3 = LogEntry("119.109.247.248", LocalDateTime.now(), "requestURL3", 200, "userAgent3", "referrer3" )

    var intialLogEntries =  Array<LogEntry>(0){LogEntry()}




    @BeforeEach fun init(){
        // Set the initialLogEntries collection to have 4 values
//        intialLogEntries.clear()
//        intialLogEntries.add(logEntry1);
//        intialLogEntries.add(logEntry1a);
//        intialLogEntries.add(logEntry2);
//        intialLogEntries.add(logEntry3);

        intialLogEntries = arrayOf(logEntry1, logEntry1a, logEntry2, logEntry3)

    }


    @Test fun testTokeniseLine() {
        val testLine = """109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
        println(testLine)
        val res = tokeniseLine(testLine)

        assertEquals(8,  res?.size) // 7 groups plus the entire string
    }


    @Test fun testTokeniseLineShortIP() {

        val testLine = """31.173.243.17 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
        println(testLine)
        val res = tokeniseLine(testLine)

        assertEquals(8,  res?.size) // 7 groups plus the entire string
    }


    @Test fun testTokeniseLineNoResponseSize() {

        val testLine = """213.150.254.81 - - [18/Dec/2015:12:44:42 +0100] "GET /images/phocagallery/almhuette/thumbs/phoca_thumb_m_wohnraum.jpg HTTP/1.1" 304 - "http://www.almhuette-raith.at/index.php?option=com_phocagallery&view=category&id=1&Itemid=53" "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)" "-""""
        println(testLine)
        val res = tokeniseLine(testLine)

        assertEquals(8,  res?.size) // 7 groups plus the entire string
    }

    @Test fun testTokeniseLineUnexpected() {

        val testLine = """213.150.254.81 - - [18/Dec/2015:12:44:12 +0100] "GET /images/phocagallery/almhuette/thumbs/phoca_thumb_m_almhuette_raith_009.jpg HTTP/1.1" 200 4264 "http://www.almhuette-raith.at/index.php?option=com_phocagallery&view=category&id=1&Itemid=53" "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)" "-""""
        println(testLine)
        val res = tokeniseLine(testLine)

        assertEquals(8,  res?.size) // 7 groups plus the entire string
    }

    @Test fun testgetUniqueRequestIPs(){
        val uniqueIPEntries = getUniqueRequestIPs(intialLogEntries.toList())
        assertEquals(3, uniqueIPEntries.size, "Should have been 3 unique IP addresses in the log")

    }

}