General
=======
This is the case study app used as a demonstration for the 1st ex in the course.
It is based on but not identical to the code used in the other course exercises with the 
addition of Spring Boot and a Spring MVC front end. 

Dependencies
-----------
The service version **CaseStudy2327** has to be running as it provides the REST service which the log files
are consumed from.

Runs as a service
-------------------
To create the version of this which is installed as a service:

1) Remove the port property from application.properties
2) Run the app with the spring-boot:run maven goal
3) Package the app (mvn)
4) Replace the version of the jar which is in C:\course2327\bin


Then make sure you put the port number back to 9090
