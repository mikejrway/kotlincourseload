package com.ltree.kotlin.server

import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

import khttp.get

const val URL="http://localhost:9090/logserver/"
/**
 * E2E test of the server. Make sure the server is running first (spring-boot:run)
 */
class LogServerControllerTests {

    @Test
    fun testServer(){
        val params = mapOf("fileName" to "split0.log")
        val r = get(URL, params=params)

        assertEquals(200, r.statusCode)
        assertEquals(22305414, r.content.size)
    }

    @Test
    fun testServerSmall(){
        val params = mapOf("fileName" to "small_access.log")
        val r = get(URL, params=params)

        assertEquals(200, r.statusCode)
        assertEquals(1286321, r.content.size)
    }

    @Test
    fun testServerHuge(){
        val params = mapOf("fileName" to "access.log")
        val r = get(URL, params=params)

        assertEquals(200, r.statusCode)
        assertEquals(679983833, r.content.size)
    }

    /**
     * Verify we get the correct data back!
     */
    @Test
    fun testData(){
        val params = mapOf("fileName" to "small_access.log")
        val r = get(URL, params=params)

        assertEquals(200, r.statusCode)
        val expected="""109.169.248.247 - - [12/Dec/2015:18:25:11 +0100] "GET /administrator/ HTTP/1.1" 200 4263 "-" "Mozilla/5.0 (Windows NT 6.0; rv:34.0) Gecko/20100101 Firefox/34.0" "-""""
        assertEquals(expected, r.text.subSequence(0, expected.length))
    }

    /**
     * Do we get the right number of lines of text back
     */

    @Test
    fun testDataLines(){
        val params = mapOf("fileName" to "small_access.log")
        val r = get(URL, params=params)

        val linesList = r.text.split('\n')
        assertEquals(6351, linesList.size)


    }

}