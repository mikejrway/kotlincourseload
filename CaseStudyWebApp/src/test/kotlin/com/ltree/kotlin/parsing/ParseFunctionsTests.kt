package com.ltree.kotlin.parsing

import junit.framework.Assert.assertEquals
import org.junit.Test

class ParseFunctionsTests {

    @Test
    fun testIpAddrToInt(){

        val inputs= arrayOf("0.0.0.1", "0.0.1.0", "0.1.0.0", "1.0.0.0")
        val expected = arrayOf(0x1,0x100, 0x10000, 0x1000000)
        var idx = 0
        for(input in inputs){
            println(input)
            var out = ipAddrToInt(input)
            assertEquals(expected[idx++], out)
        }


    }

}