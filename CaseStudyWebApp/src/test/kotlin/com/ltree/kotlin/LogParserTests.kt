package com.ltree.kotlin

import junit.framework.Assert.assertTrue
import org.junit.Test

class LogParserTests {

    @Test
    fun testFilterByUserAgent(){
        val parser = LogParser()
        parser.loadLogEntries()

        val results = parser.filterLogsByUserAgent()
        assertTrue(results.size > 1)
    }

    @Test
    fun testFilterByReq(){
        val parser = LogParser()
        parser.loadLogEntries()

        val results = parser.filterLogsByUniqueRequestIP()
        assertTrue(results.size > 1)
    }
}