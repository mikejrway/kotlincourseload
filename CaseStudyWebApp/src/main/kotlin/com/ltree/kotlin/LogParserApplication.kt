package com.ltree.kotlin

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@SpringBootApplication

class LogParserApplication

fun main(args: Array<String>) {
	runApplication<LogParserApplication>(*args) {
		setBannerMode(Banner.Mode.OFF)
	}
}