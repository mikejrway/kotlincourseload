package com.ltree.kotlin


interface ILogParser {

    fun loadLogEntries() : Result

    fun filterLogsByUniqueRequestIP(): List<ReqCount>

    fun filterLogsByUserAgent(): List<UACount>

}