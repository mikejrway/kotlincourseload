package com.ltree.kotlin.server

import com.ltree.kotlin.ILogParser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping

@Controller
@Scope("session")
/**
 * This is the MVC controller which serves the web pages for the application and handles the
 * submission of the commands
 */
class HtmlController {

    @Autowired
    private lateinit var logParser: ILogParser

    @GetMapping("/")
    fun blog(model: Model): String {
        defaultModel(model)
        return "home"
    }


    @PostMapping("/load/")
    fun loadLogFiles(model: Model): String {
        defaultModel(model)
        val result = logParser.loadLogEntries()
        model["message"]= "Loaded ${result.fileCount} files. ${result.logEntryCount} entries in total"
        model["result"]=result
        model["entries"]=false
        model["enableFilters"]=true
        return "home"
    }

    @PostMapping("/filter/{type}")
    fun filterLogFiles(@PathVariable("type") filterType: String, model: Model): String {
        defaultModel(model)
        var result: List<Any>
        when(filterType){
            "ReqIp" -> result = byReq(model)
            "UA" -> result = byUA(model)
            else -> {
                model["title"] = "Error Page"
                return "error"
            }
        }
        model["entries"]=result
        model["enableFilters"]=true
        return "home"
    }

    private fun byReq(model: Model): List<Any>{
        model["ByReq"]=true
        return logParser.filterLogsByUniqueRequestIP().subList(0,20)
    }

    private fun byUA(model: Model): List<Any>{
        model["ByUA"]=true
        return logParser.filterLogsByUserAgent().subList(0,20)
    }

    private fun defaultModel(model: Model){
        model["title"] = "Log Miner"
        model["message"]=""
        model["result"]=false
        model["entries"]=false
        model["enableFilters"]=false
        model["ByUA"]=false
        model["ByReq"]=false
    }
}