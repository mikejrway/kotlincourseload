package com.ltree.kotlin.server


import org.apache.tomcat.util.http.fileupload.IOUtils
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import java.io.File
import java.time.LocalDateTime
import java.time.Month
import java.util.concurrent.atomic.AtomicLong
import javax.servlet.http.HttpServletResponse

const val BASE_FILE_PATH="""../sampledata/"""

/**
 * REST controller for serving the log file contents
 */
@RestController
class LogServerController {


    @GetMapping("/logserver", produces=["text/plain"])
    fun geLogFiles(@RequestParam(value = "fileName", defaultValue = "small_access.log") fileName: String, response: HttpServletResponse) {

            // Using Thread.sleep not delay due to complexity of making Spring RestController work with coroutines
        try {
            Thread.sleep(2000)
        } catch (e: InterruptedException){
            // Do nothing
        }

            val inputStream = File("${BASE_FILE_PATH}${fileName}").inputStream()
            IOUtils.copy(inputStream, response.outputStream)
            response.flushBuffer()

    }

    @GetMapping("/logentries")
    fun getLogEntries(@RequestParam(value = "fileName") fileName: String?) : List<LogEntry>? {
        // Load stuff from a file
        val logEntries: List<LogEntry>? = null
        return logEntries
    }
}


/** Dummy class -- used to create an entry in the course notes */
data class LogEntry( val ipAddress: String)

