package com.ltree.kotlin


import com.ltree.kotlin.model.LogEntry
import com.ltree.kotlin.parsing.buildLogEntry
import com.ltree.kotlin.parsing.tokeniseLine
import khttp.get
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import org.springframework.stereotype.Component
import java.io.File
import java.io.StringReader
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis

const val URL="http://localhost:9090/logserver/"

@Component
class LogParser : ILogParser {

    private val logFileLines: MutableList<String> = ArrayList()

    private val logEntries:MutableList<LogEntry> = ArrayList()

    var channel = Channel<LogEntry>()


    override fun loadLogEntries() : Result {
        channel = Channel()
        println("Loading Log Files")
        val receiverJob = GlobalScope.launch{
            channelReceiver()
        }
        var elapsedTime = 0L

        elapsedTime = measureTimeMillis {

            runBlocking {
                //+
                for (i in 0..1) { // Was 10 -- truncated for now
                    launch{
                        //+

                        loadLogEntriesFromServer("split$i.log")
                    } // End of launch block
                }
            } // End runBlocking
        } // End measure time block

        // Execution only gets to here once all the coroutines launched in runBlocking have completed
        channel.close()
        runBlocking {
            receiverJob.join()
        }

        println("Processing Complete ${logEntries.size} entries processed.")
        val seconds = TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.MILLISECONDS);
        println("Elapsed time: $seconds (seconds) $elapsedTime (milliseconds)")

        return Result(10, logEntries.size )
    }


    override fun filterLogsByUniqueRequestIP(): List<ReqCount> {
        var distinctEntries =getUniqueRequestIPs(logEntries)
        //distinctEntries = distinctEntries.subList(0,10000) // Temp -- truncate the list
        val entriesByReqIp = ArrayList<ReqCount>()
        val elapsedTime = measureTimeMillis {
            for (entry in distinctEntries) {
                val count = logEntries.count { it.ipAsInt == entry.ipAsInt }
                entriesByReqIp.add(ReqCount(entry.ipAddr, count))
            }
        }
        println("Elapsed time: $elapsedTime (milliseconds)")
        entriesByReqIp.sortBy { it.count }
        entriesByReqIp.reverse()
        return entriesByReqIp

    }

    override fun filterLogsByUserAgent(): List<UACount> {
        val distinctEntries = logEntries.distinctBy { it.userAgent }
        println(">>>>>>>>>> Size of distinctEntries = ${distinctEntries.size}")
        val entriesByUA = ArrayList<UACount>()
        // Now count each one
        for(entry in distinctEntries){
            val count = logEntries.count { it.userAgent == entry.userAgent }
            entriesByUA.add(UACount(entry.userAgent, count))
        }
        entriesByUA.sortBy { it.count }
        entriesByUA.reverse()
        return entriesByUA
    }


    private suspend fun channelReceiver(){
        channel.consumeEach { logEntries.add(it) }
    }
    private suspend fun loadLogEntriesFromServer( fileName: String ){
        println("Loading file $fileName")

        val paramMap = mapOf("fileName" to fileName)
        withContext(Dispatchers.IO) {
            //+
            val response = get(URL, params = paramMap)
            val reader = StringReader(response.text)

            reader.use {
                reader ->
                reader.forEachLine {
                    line ->
                    launch {
                        channel.send(processLogEntry(line))
                    }
                }
            }
        }// end withContext
    }

    /**
     * Load the log entries from a file
     */
    internal fun loadLogEntries( fileName: String ){
        File(fileName).reader().use {
            it.forEachLine {
                logFileLines.add(it)
            }
        }
    }

    /**
     * Function to process a single line in the log file. Eventually, this will evolve to return a new model object
     */
    internal fun processLogEntry( logLine: String): LogEntry {
        val tokens = tokeniseLine(logLine)
        var logEntry = LogEntry()
        if(tokens != null)
            logEntry = buildLogEntry(tokens)
        return logEntry
    }






    /**
     * The function you will write returns an Array<LogEntry> where each entry has a dstinct (unique) IP address
     */
    internal fun getUniqueRequestIPs(logEntries: MutableList<LogEntry>): List<LogEntry>{
        return logEntries.distinctBy { it.ipAsInt }
    }

} // End of the class

/**
 * Data classes for communication with the Web servlet as return values
 */
data class Result(val fileCount: Int, val logEntryCount: Int)
data class UACount(val uaName: CharSequence, val count:Int)
data class ReqCount(val ipAddr: CharSequence, val count:Int)