import java.util.*

class Conditions_b constructor(var windSpeedParam: Double, var locationParam: String )

class Conditions_a constructor(windSpeedParam: Double, locationParam: String ) {
    var windSpeed : Double
    val location: String

    init {
        windSpeed = windSpeedParam
        location = locationParam
    }

    constructor() : this(9.9, "Home")
}


class Conditions( val temperature: Double = 20.0) {
    var windSpeed : Double = 0.0
    val location: String = "NYC"
    var humidity: Double=50.0


    fun updateWeather(location: String): Unit{
        fetchFromServer(location)
        return
    }


    fun updateWeather(location: String, time: Calendar){
        println("Fetching weather ${time}")
        fetchFromServer(location)
        return
    }

    fun fetchFromServer(location: String){

    }
}
fun main(){
    println(Manager("Bill", "CTO",6).toString())
    val conditions = Conditions()
    updateWeather("HERE")
    println(conditions.windSpeed)

}


fun updateWeather(location: String, time: Calendar = Calendar.getInstance()){
    Conditions().fetchFromServer(location)
    return
}

const val PLANCK_LENGTH=1.6E-35
