interface Named {
    val age: Int // Properties are either abstract or provide a getter -- no backing fields
        get() = 42

    val name: String
}

interface Person : Named {
    val firstName: String
    val lastName: String

    override val name: String get() = "$firstName $lastName"
}

//data class Employee(
//    // implementing 'name' is not required
//    override val firstName: String,
//    override val lastName: String,
//    val position: Position
//) : Person



interface Manageable {

    val location: String

    fun update() : Boolean

    fun shutDown(interval: Int ) : Boolean
}

interface Storable {
    fun saveStation(location: String): Unit
}


class AStation() : Manageable, Storable {
    override fun saveStation(location: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override val location: String = ""
    override fun update(): Boolean {
        TODO("not implemented")
    }
    override fun shutDown(interval: Int): Boolean {
        TODO("not implemented")
    }
}

class BStation() : Storable {
    override fun saveStation(location: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

val stations = arrayOf(AStation(), BStation(), AStation())
fun shutdownStations(){
    for(station in stations){
        if(station is Manageable){
            station.shutDown(10_000)
        }
    }
}