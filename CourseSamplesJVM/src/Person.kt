package com.ltree.kotlin.samples

// Default class visibility is public
// Use val or var in the constructor to turn parameters into properties
// Using private as a modifier works
open class Person ( var name: String) {
    // Must use explicit override modifier
    override fun toString() : String {
        return this.name
    }

    open fun calcPay() : Double {
        return 0.0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Person

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }


}

class SimplePerson ( ) {

    var name: String = ""

    override fun toString() : String {
        return this.name
    }

    open fun calcPay() : Double {
        return 0.0
    }
}


class Person2  {

    var name: String = ""

    // Must use explicit override modifier
    override fun toString() : String {
        return this.name
    }
}


class Person3 constructor(nameParam: String, payParam: Double) {
    var pay : Double
    val name: String

    init {
        pay = payParam
        name = nameParam
    }
}

class Person4 (val name: String, var pay: Double) {
    constructor() : this("Fred", 9.00)
}


data class Person5 (val name: String, var pay: Double) {
    constructor() : this("Fred", 9.00)

    fun doStuff(){}
}

open class Employee ( name:String) : Person(name) {
    lateinit var nickName: String
    var jobTitle: String
    open var pay = 100.0

    init {
        jobTitle=""
    }

    override fun calcPay() : Double {
        return 99.99
    }

    override fun toString(): String {
        return "$name is productive"
    }

    fun promote(newJob: String, increment: Double){

    }
}

class Manager (name: String) : Employee(name){
    override var pay: Double = 22.3;
}

// Extension function for Employee
fun Employee.promote(jobTitle: String, increment: Double ){
    this.jobTitle = jobTitle
    this.pay += increment
}
// A complete class declaration!
class Mammal

fun main(){
    var bill = Employee("bill")
    println(bill.nickName)
    bill.promote("CEO", 50_000.00)
//    println(bill)
//    println(bill.name + "Prop access")
//    println(bill.toString2())
//
//    var person: Person = bill as Person
//    println( "Employee pay ${bill.calcPay()}")
//
//    // Calling calcPay on person still calls the overridden method
//    // E.g. resolved based on the real type not the static type
//    println( "Person pay ${person.calcPay()}")
//
//    // This cast generates a warning not an error at compile time
//    // At runtime it throws a ClassCastException
//    val dog : Mammal = person as Mammal
//
//    println(dog)

    val o = Any()
    println(o)
    val person2 = Person2()
    person2.name = "Justin Time"
    println(person2)
}

// A shadow function --  not called "normally"
//"If a class has a member function, and an extension function is defined which has the same receiver type, the same name is applicable to given arguments, the member always wins."

fun Person.toString(): String{
    return this.name + "Extended"

}

fun Person.toString2(): String{
    return this.name + "Extended v2"
}