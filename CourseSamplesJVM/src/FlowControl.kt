import kotlin.random.Random
import kotlin.system.exitProcess

fun main(){
    //println("Hello World")
    //return
    repeat()
    exitProcess(1)
}

//fun main(){
////    ifAsAssignment()
////    ifElse()
////    statementBlocks()
////    simpleWhen()
////    computedWhen()
//    rangeOrCollection()
//}

fun ifAsAssignment(){
    println("================= ifAsAssignment ===============")
    val temp=28
    val hotOrCold = if( temp > 30 ) "Hot" else "Cool"
    println(hotOrCold)
}

fun ifElse() {
    println("================= ifElse ===============")
    val windSpeed = Random.nextInt(0, 999)

    if ( windSpeed > 80.0) {
        println("Severe Storm")
    } else {
        println("Don't panic")
    }

}

fun statementBlocks() {
    println("================= statementBlocks ===============")
    val x = Random.nextInt(0,9999)
    val y = Random.nextInt(0, 9999)

    val max = if (x > y) {
        println("a value is max")
        x
    } else {
        println("b value is max")
        y
    }

    println ("Max is $max")

}


fun simpleWhen(){
    println("================= simpleWhen ===============")
    val x = Random.nextInt(1,4)
    when (x) {
        1 -> print("x is 1")
        2 -> print("x is 2")
        3,4 -> print("x is 3 or 4")
        else -> print("x is not 1,2,3 or 4")
    }
}

fun computedWhen(){
    val x = Random.nextInt(1,4)
    val y =42
    when (x) {
        computeAValue(y) -> print("x matches computeAValue(y) ")
        else -> print("some other condition")
    }

}

fun rangeOrCollection(){
    val collection: Array<Int> = arrayOf( 100, 200, 400)
    val x = 21
    when (x) {
        in 1..10 -> print("x is in the range 1..10")
        !in collection -> print("x is in the collection")
        !in 10..20 -> print("x is outside the range 10 ..20")
        else -> print("x what is x?")
    }
}


fun whenTypeChecking(value: Any): Int{

   val res = when(value){
        is String -> value.toInt()
        else -> 0
    }
    return res
}


fun forSimple(){
    val aCharacter : Char = 'Z'
    val intArray: IntArray = intArrayOf( 100, 200, 400)
    val anArray: Array<Int> = arrayOf( 100, 200, 400)
    for( item: Int in anArray ){
        print(item)
    }

    for( item: Int in anArray ) print(item)

}
private fun computeAValue(y: Int) : Int{
   return y
}

fun stuff(): Unit {
    val a : Int = 0
    val b : Int = 0
    //return 42 * 99
    val length = a*a + b*b
}

fun whileLoop() {
    var count = 1
    while (count <= 10) {
        println("3 times $count is ${3 * count}")
        count += 1
    }
}

fun dowhileLoop() {
    var count = 1
    do {
        println("3 times $count is ${3 * count}")
        count += 1
        if(count == 4) break
    } while (count <= 10)

}

fun breakContinue(){
    var count = 1
    while (count++ <= 10) {
        if (isBad(count)) {
            break
        }
        if (isGood(count)) {
            continue
        }
        // Loop processing here
    }
}

fun labelsAndLoops(){
    loop@ for (i in 1..10) {
        for (j in 1..10) {
            if (isBad(j)) break@loop
            if (isGood(j)) continue@loop
        }
    }
}

fun isBad(i: Int) : Boolean{

    return (i > 2)
}

fun isGood(i: Int) : Boolean{

    return (i > 2)
}

fun repeat(){
    repeat(10) {
        val count = it
        println("The count is $count")
    }
}