data class Station( val location: String, val conditions: Conditions) {

    constructor() : this( "Home", Conditions())

    //var reports: ArrayList<String> = arrayListOf()
    var reports: Array<String> = arrayOf()

    /**
     * This function overrides the + operator allowing us to add a
     * String to the list of reports within a weather station.
     * As can be seen from main() this supports both + and +=
     */

    operator fun plus(report: String): Station {
        //reports = reports.plus(report)
        reports += report
        return this
    }

    override fun toString(): String{
        val output = StringBuilder()
        for(report in reports){
            output.append("$report, ")
        }
        return output.toString()
    }
}

fun main(){
    var station = Station()
    station = station + "Sunny at home"
    station += "Raining at work"

    println(">>>>>>>>" + station)
}