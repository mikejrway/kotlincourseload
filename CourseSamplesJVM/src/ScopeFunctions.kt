fun main(){
    println("Let")
    doLet()
    println("Run")
    doRun()
    println("Run - No Context")
    doRunNoContext()
    println("With")
    doWith()
    println("Apply")
    doApply()
    println("Also")
    doAlso()

}



/*
    Context supplied as the argument, return is the lambda return
 */
private fun doLet(){

    val updatedWeather = WeatherConditions().let {
        println(it.temp)
        WeatherConditions(it.temp + 1.5) // Return value
    }
    println(updatedWeather.temp)
}
/*
    Context supplied as this (receiver), return is the lambda return
 */
private fun doRun(){

    val updatedWeather = WeatherConditions().run {
        println(this.temp)
        println(temp) // Or, just drop the this
        WeatherConditions(temp + 1.5) // Return value
    }
    println(updatedWeather.temp)
}

private fun doRunNoContext(){

    val updatedWeather = run {
        WeatherConditions(temp = 21.5) // Return value
    }
    println(updatedWeather.temp)
}

/*
    Context supplied explicitly as an argument to with, return is the lambda return
 */
private fun doWith(){
        // with passes the context object as an argument
    val updatedWeather = with(WeatherConditions()) {
        println(this.temp)
        println(temp) // Or, just drop the this
        WeatherConditions(temp + 1.5) // Return value
    }
    println(updatedWeather.temp)
}

/*
    Context supplied as this (receiver), return is the context object
 */
private fun doApply(){
    // with passes the context object as an argument
    val conditions = WeatherConditions().apply {
        println(this.temp)
        println(temp) // Or, just drop the this
        temp += 1.5 // Return value
    }
    println(conditions.temp)
}


/*
    Context supplied as the argument, return is the context object
 */

private fun doAlso(){

    val conditions = WeatherConditions().also {
        println(it.temp)
        it.temp += 1.5 // Return value
    }
    println(conditions.temp)
}


data class WeatherConditions(var temp: Double = 20.0, var percipitation: Double = 0.0)