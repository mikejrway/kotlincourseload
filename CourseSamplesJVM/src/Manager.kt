import com.ltree.kotlin.samples.Employee

// Init of grade is most concise
open class Manager constructor ( name: String, roleArg: String, private val grade: Int) : Employee(name){

    // Most explicit (and verbose) initialization of name
    //val name: String

    init {
        super.name = name
    }


    init {
        // Can use role here but not this.role as this.role is not yet initialized
        pay = roleArg[0].toInt()  * 99.99 // Just for fun - not logical in any way! -- runtime exception
    }

    // Initilization of role more concisely but compiler recommends moving it to the constructor declaration
    val role = roleArg

    var department: String = ""
        get() {
            return "Current $field"
        }
        set(value){
            if(isValid(value)) field = value
        }

    private fun isValid(dept: String): Boolean{
        val dep = lookupDepartment(dept)
        return dep != null
    }

    private fun isValid2(dept: String) =  lookupDepartment(dept) != null

    private fun lookupDepartment(dept: String) = ""


    override fun toString() : String {
        return "Manager $name , role: $role , grade: $grade, pay: $pay"
    }
}

fun main(){
    val manager = Manager("Fred", "CTO", 8)
    manager.department="Exec"
    println(manager.department)
}