import com.learningtree.kotlin.model.LogEntry
import kotlinx.coroutines.*
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine



/**
 * Run this with VM option -Dkotlinx.coroutines.debug
 */

fun main(args: Array<String>) {
    //suspendResume()
    //nestedCoroutines()
    nestedCoroutinesWithGlobalScope()
}

private fun suspendResume() {
    var continuation: Continuation<Unit>? = null
    log("main(): launch")
    GlobalScope.launch(Dispatchers.Unconfined) {
        log("Coroutine: started")
        suspendCoroutine<Unit> {
            // Code in the block is executed and the the coroutine suspends
            log("Coroutine: suspended")
            continuation = it
        }
        // This line gets executed after the resume
        log("Coroutine: resumed")
    }
    log("main(): resume continuation")
    // In this single threaded example: the call to resume is actually executing the last part of the coroutine
    continuation!!.resume(Unit)
    log("main(): back after resume")
}

private fun nestedCoroutines() = runBlocking{
    val parent = launch {
        val child = launch {
            while(true) {
                delay(100)
                println("child running")
            }
        }
        while(true){
            delay(300)
            println("running")
        }
    }
    val killer = launch {
        delay(600)
        parent.cancel()
    }
    println("end")
}

private fun nestedCoroutinesWithGlobalScope() = runBlocking{
    var child : Job? = null
    val parent = launch {
        child = GlobalScope.launch {
            while(true){
                delay(300)
                println("child running")
            }
        }
        while(true){
            delay(300)
            println("running")
        }
    }
    val killer = launch {
        delay(600); parent.cancel()
    }
    while(true) {
        if (child?.isActive == false)
            break
        delay(100)
    }
     println("end")

}

/**
 * Example function that converts an async KHTTP call into a suspending function
 */
private suspend fun loadLogEntry() : LogEntry
{
    return suspendCoroutine<LogEntry> {continuation ->
        khttp.async.get("https://www.logserver.com/",
            onResponse = {
                continuation.resume(jsonObject as LogEntry)
            },
            onError = {
                continuation.resumeWithException(Exception(message))
            })
    }
}