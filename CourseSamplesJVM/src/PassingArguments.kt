import com.ltree.kotlin.samples.Person

fun main(){

//    var a = Number(5)
//    addOne(a)
    val p = Person("Issac")
    changeName(p)
    println(p)

}

//fun addOne( a : Number ){
//    a.num++
//    a = Number(9)
//}
//
//fun addOne( a : Int ){
//    a.v
//}

fun changeName(p: Person){
    p.name="Galileo"
}

fun changePerson(p: Person){
    // p = Person("Galileo") // Compile error here (p is a val)
}


class Number (var num : Int)