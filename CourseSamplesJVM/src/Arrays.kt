fun main(){
    println(intArray[1])
    intArray[1] = 5
    println(intArray[1])

    intArray += 99
    println(intArray[intArray.size -1])

    try {
        intArray[10] = 5   // Bang!
    } catch (e: Exception){
        // Delegate to Java
        e.printStackTrace()
    }

    for(s in stringArray){
        println(s)
    }

    addIntToArray()
    //getStringFromArray()

    anArray.map{println(it)}
}

var intArray: IntArray = intArrayOf( 100, 200, 400)

val stringArray = Array<String>(10) { "String_$it" }

val anyArray = arrayOf<Any>(99.0, "Ninety Nine")

val anArray: Array<Int> = arrayOf( 100, 200, 400)

var a1 = arrayOf(0,9,0)
var a2 = arrayOf(0,2,4,5)

var readings: Array<Array<Int>> = arrayOf(a1,a2)

var readings2: Array<Array<Int>> = arrayOf(arrayOf(0,9,0),arrayOf(0,2,4,5))

fun addIntToArray(){
    anyArray[0] = 42
}

fun getStringFromArray() : String{
    return anyArray[0] as String // Fails as the cast of 99.9 to a String is impossible!
}