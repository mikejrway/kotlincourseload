fun main(){

    val name = "Issac Newton"
    val cpyName = name
    println(cpyName)

    greeter(myFunc, "A message")
    greeter(::myFunc2, "A message from func 2")
}

val myFunc = fun (greeting: String){ println(greeting)}

fun myFunc2 (greeting: String){ println(greeting)}

fun greeter( theFunc: (String) -> Unit, msg: String ){
    theFunc(msg)
}