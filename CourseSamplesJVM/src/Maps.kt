fun main(){

    val map1 = HashMap<Int,String>()
    val map: Map<Int, String> = mapOf<Int, String>(Pair(1,"one"), Pair(2,"two"))

    if(map is MutableMap<Int, String>) {
        var mmap = map as MutableMap

        mmap.put(3, "three")
        println(map.get(3))
    } else {
        println("Map is immutable")
        var mmap = map.toMutableMap()
    }
}


