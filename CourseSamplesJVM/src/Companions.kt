fun main(){

    val car = LogEntry2.Factory.makeEntry("1.2.3.4", "Unusual IP?")
    println(LogEntry2.entries.size)
}


class LogEntry2( val ipAddr: CharSequence, val text: CharSequence) {
    companion object Factory {
        val entries = mutableListOf<LogEntry2>()

        fun makeEntry( ipAddr: CharSequence,  text: CharSequence ): LogEntry2{
            val entry = LogEntry2(ipAddr, text)
            entries.add(entry)
            return entry
        }
    }

    fun makeEntry( ipAddr: CharSequence,  text: CharSequence ): LogEntry2{
        return LogEntry2(ipAddr, text)
    }

}