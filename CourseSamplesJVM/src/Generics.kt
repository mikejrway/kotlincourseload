fun main(){
    
    val box = Wrapper<Int>(3)
    println(box.value)

    val box2 = Wrapper(3)
    println(box2.value)    
}


class Wrapper<T>(t:T){
    var value=t

    override fun toString(): String {
        return "Wrapped value is $value"
    }
}

