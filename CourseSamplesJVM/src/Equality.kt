import com.ltree.kotlin.samples.Person

fun main(){
    var issac = Person("Issac Newton")
    var cpyIssac = issac
    var imposter = Person("Issac Newton")

    println(issac.equals(imposter))
    println(issac == imposter)

    println(issac === imposter) // false
    println(issac === cpyIssac) // true


}


