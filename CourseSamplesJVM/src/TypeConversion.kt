fun main(){
    mixedTypeMath()
}

fun mixedTypeMath(){
    val a: Int = 1
    val b: Float = 3.0F
    val c = a * b
    printType(c)
}

fun printType(x:Any){
    val type = when(x){
        is Int -> "Int"
        is Long -> "Long"
        is Float -> "Float"
        is Double -> "Double"
        else -> "not sure"
    }
    println(type)
}