interface LineCounter {
    fun countLines(): Int
}

class FileLogReader : LineCounter by LineCounterImpl() {

}

class LineCounterImpl : LineCounter {
    override fun countLines(): Int {
        return 1
    }
}
