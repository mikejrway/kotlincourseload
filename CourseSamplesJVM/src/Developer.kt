import com.ltree.kotlin.samples.Employee

class Developer constructor (name: String) : Employee(name) {

    override fun toString(): String {
        return  "$name is overworked and underpaid"
        //return super.toString()
    }
}