import com.learningtree.kotlin.model.LogEntry
import khttp.async
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

// Example of a function with a receiver
var sayHiRepeatedly: String.(Int) -> Unit = {
        n -> repeat(n)
    println("Hello $this")
}


val longWinded: (x: Int, y: String) -> Double = fun (x,y) : Double {
    println("$x, $y")
    return 99.0
}

fun normal(x: Int, y: String) : Double {
    println("$x, $y")
    return 99.0
}

val lambda: (x: Int, y: String) -> Double = {x,y -> println("$x, $y"); 99.0}


fun main(){
    //sayHiRepeatedly("Mike", 5)
    findIdealWeather()
}



fun findIdealWeather( ){

    val myFn = {condition: Conditions -> condition.temperature > 25}

    var conditions = listOf(Conditions(18.0), Conditions(25.1), Conditions(29.0))

    conditions.filter(myFn)

//    val warmPlaces: List<Conditions> =
//        conditions.filter({condition -> condition.temperature > 25})


    val warmPlaces: List<Conditions> = conditions.filter{condition: Conditions -> condition.temperature > 25}

    //val warmPlaces: List<Conditions> = conditions.filter{it.temperature > 25}

    for(condition in warmPlaces){
        println(condition.temperature)

    }
}

fun sortStrings() {
    val input = listOf("Sun", "Rain", "Hail", "Snow")
    val output = input.filter ({ s -> s.startsWith("S") })

}


inline fun <T> process(item: T, printer: (T) -> Unit ) {
    printer(item)
}

fun printerProcess(){
    val logEntry = LogEntry("1234")
    process(logEntry){println(it.ipAddr)}

}


suspend fun loadLogEntries() = runBlocking {

    for (i in 0..10) {
        async{loadLogEntriesFromServer("split$i.log")}
    }
}

private fun loadLogEntriesFromServer(fileName: String){

}

suspend fun doSomethingUsefulOne(): Int {
    delay(1000L) // pretend we are doing something useful here
    return 13
}

suspend fun doSomethingUsefulTwo(): Int {
    delay(1000L) // pretend we are doing something useful here, too
    return 29
}

fun sequential() = runBlocking {
    val time = measureTimeMillis {
        val one = doSomethingUsefulOne()
        val two = doSomethingUsefulTwo()
        println("The answer is ${one + two}")
    }
    println("Completed in $time ms")
}
