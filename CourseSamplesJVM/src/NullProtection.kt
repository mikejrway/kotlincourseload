


fun main() {
//    val logEntries: ArrayList<Int>? = null
//
//    val counta: Int = if (logEntries != null) logEntries.size else -1
//    val countb = logEntries?.size ?: -1
//
//    println("Count A: $counta, Count B $countb")
    safeCalls()
}


fun safeCalls(){
    var helloStr: String? = "Hi there"
    println(helloStr?.length)
    //println(helloStr.length) // won't compile
    if(helloStr != null) println(helloStr.length)
    helloStr = null
    println(helloStr?.length)

}