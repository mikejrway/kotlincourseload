sealed class LogEntry
data class ApacheLogEntry(val version: String) : LogEntry()
data class IISLogEntry(val userName: String) : LogEntry()



