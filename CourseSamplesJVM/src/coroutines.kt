import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlin.random.Random

fun main(){
    //example1()
    //debugFunc()
//    runBlocking {
//        doSomething(this)
//    }
//    example3()
    producerDemo()
}


fun example1() {
    GlobalScope.launch {

        delay(100)
        print("World!")
    }
    runBlocking {
        print("Hello ")
        delay(500)
    }
}

fun example2() {
    runBlocking {
        var job = launch {
            delay(100)
            print("World!")
            doSomething(this)
        }
        print("Hello ")
        job.join()
    }
}

/**
 * This example generates no output! The application finished before either coroutine does any work!
 */
fun example3() {
    GlobalScope.launch {

        delay(100)
        print("World!")
    }
    GlobalScope.launch {
        print("Hello ")
        delay(500)
    }
}



suspend fun doSomething(scope: CoroutineScope) {
    // launch ten coroutines for a demo, each working for a different time
    repeat(10) { i ->
        coroutineScope {
            // launch must either be GlobalScope or in the context of another scope ??
            scope.launch {
                delay((i + 1) * 200L) // variable delay 200ms, 400ms, ... etc
                println("Coroutine $i is done")
            }
        }
    }
}

/**
 * Run this with VM option -Dkotlinx.coroutines.debug
 */
fun debugFunc() = runBlocking<Unit> {
    //sampleStart
    val a = async {
        log("I've been launched and will return 99")
        99
    }
    val b = async {
        log("Now it's my turn. I'll return 42")
        42
    }
    log("The answer is ${a.await() * b.await()}")
//sampleEnd
}

fun log(msg: String) = println("[${Thread.currentThread().name}] $msg")


fun asyncExample() = runBlocking<Unit> {
    val a = async {
        firstExample()
    }
    val b = async {
        secondExample()
    }
    println("Result ${a.await() * b.await()}")
}

private suspend fun secondExample(): Int {
    println("Now it's my turn. I'll return 42")
    delay(100)
    return 42
}

private suspend fun firstExample(): Int {
    println("I've been launched and will return 99")
    delay(200)
    return 99
}

var produceWeather = true

fun producerDemo(){
    runBlocking{
        val channel = produceWeatherReports()
        launch{consumeWeather(channel)}
        delay(10_000)
        println("Stop weather production")
        produceWeather = false
    }
}
/**
 * Demonstrate the channel producer pattern
 *
 * Note the withTimeout code. If this is enabled the consumer does not terminate!
 * This is because withTimeout stops by throwing a TimeoutException
 * Replacing withTimeout with withTimeoutOrNull fixes the issue - no longer throws an exception
 */
fun CoroutineScope.produceWeatherReports() : ReceiveChannel<String> = produce {
    val weatherStrings = arrayOf("Sunny", "Raining", "Snowing", "Cloudy")
    withTimeoutOrNull(5_000) {
        while (produceWeather) {
            val idx = Random.nextInt(0, weatherStrings.size - 1)
            send(weatherStrings[idx])
            delay(500)
        }
    }
}

suspend fun consumeWeather(channel: ReceiveChannel<String>){
    channel.consumeEach {
        println(it)
    }
    println("No more weather")
}

suspend fun timeoutDemo(): Unit{
    withTimeoutOrNull(5_000) {
        // Things to do
    }
}