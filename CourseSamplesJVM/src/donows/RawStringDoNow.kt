package donows

fun main(){
    val aString = """
        |Here is
        |some stuff
            |that should print with new line chars
                |and is badly laid out in the code
    """.trimMargin()

    println(aString)
}