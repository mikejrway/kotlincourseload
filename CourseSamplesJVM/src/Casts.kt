/**
 * Simple classes to illustrate cast issues
 */
open class DataStation(val name: String)
class ShipStation(name:String) : DataStation(name)
class GroundStation(name:String) : DataStation(name)
class BallonStation(name:String) : DataStation(name){
    var altitude = 0.0
}


/**
 * Demonstrates the use of is and the smart cast
 */
fun main(){

    val stationData: Array<DataStation> = arrayOf(ShipStation("Lucitania"), BallonStation("Polar"), GroundStation("LHR"))

    for(station in stationData){
        if(station is BallonStation){
            val balloon = station as BallonStation
            println(balloon.altitude)
        }
    }
}

fun primitiveCasting(){
    val a = 2.0 // Double
    val b: Float = a.toFloat()

    val c = 2.0F //Float

}