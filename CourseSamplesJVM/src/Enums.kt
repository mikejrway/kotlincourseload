enum class WindDirection {
    NORTH, SOUTH, WEST, EAST
}

fun main(){
    val dir = WindDirection2.EAST
    dir.x = 10
    println(WindDirection2.WEST.x)
}

enum class WindDirection2(val bearing: Int) {
    NORTH(0), SOUTH(180), WEST(270), EAST(90);
    var x = 9

}