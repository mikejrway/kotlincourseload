import java.io.File
import java.io.FileNotFoundException
import java.io.InputStreamReader

fun main(){
    println("START >>>>>>>>>>>>>>>>>>>>>>>>>>>")
    //readFileLines()
    readFileLines_Use()

    println("FINISH >>>>>>>>>>>>>>>>>>>>>>>>>>>")

    //writeFileLines()
}

/**
 * Read all the lines in a file
 * This way we must manually close the resource
 * The function may well throw exceptions which we have to catch somewhere!
 */
fun readFileLines() {
    val reader = File("src/FileIO.kt").reader()
    reader.forEachLine { println(it) }
    reader.close()
}


/**
 * Read all the lines in a file
 * This way we must manually close the resource
 * The function may well throw exceptions which we have to catch somewhere!
 */
fun readFileLinesWithExceptionHandling() {
    var reader: InputStreamReader? = null

    try{
        reader = File("src/FileIO.kt").reader()
        reader.forEachLine { println(it) }
    } catch (e: FileNotFoundException) {
        println("Could not find the file")
    } catch (e: Exception) {
        println("Some other exception occurred")
        e.printStackTrace()
    } finally {
        reader?.close()
    }

}



fun readFileLines_Use() {
    try{
        File("src/FileIO1.kt").reader().use {
            it.forEachLine { println(it) }
        }
    } catch (e: Exception) {
        println("Some other exception occurred")
        e.printStackTrace()
    }
}

fun writeFileLines() {
    val writer = File("Copy.txt").writer()
    val reader = File("src/FileIO.kt").reader()
    reader.forEachLine { writer.write(it) }
    reader.close()
    writer.close()
}