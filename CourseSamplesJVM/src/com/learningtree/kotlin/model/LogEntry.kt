package com.learningtree.kotlin.model

import java.time.LocalDateTime
import java.time.Month

data class LogEntry(val ipAddr: CharSequence,
                    val dateTime: LocalDateTime = LocalDateTime.of(2020, Month.APRIL, 1, 1,1),
                    val requestURL: CharSequence = "",
                    val responseCode: Int= 200,
                    val userAgent: CharSequence= "",
                    val referrer: CharSequence = "") {

    constructor() : this("10.1.1.1",
        LocalDateTime.of(1970, Month.JANUARY, 1, 1,1),
        "localhost",
        200,
        "Chrome",
        "")

    override fun toString(): String {
        return """LogEntry IP: $ipAddr, Timestamp: $dateTime, Requester: $requestURL, User Agent: $userAgent, "Referrer: $referrer, Response Code: $responseCode"""
    }

}