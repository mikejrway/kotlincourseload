fun main() {
    // Kotlin figures out correctly that this is a single statement


    blockStatement()
    autoIncDec()

    val res = addThemUp( 1, 2L, 3.0, 4.0F)
    println(res)
    var res2 = addThemUp(c=3.0, d=4.0F, a=1, b=2L)



}


fun addThemUp( a: Int, b: Long, c: Double = 2.0, d: Float = 9.9F) : Double {
    return a + b + c + d
}

fun blockStatement() {
    run {
        val a: Double = 3.0
        val b: Double = 4.0
        val c: Double
        c = Math.sqrt(a * a + b * b)
        println("The answer is $c")

    }

}

fun autoIncDec() {
    var i = 10
    var j = i++
    var k = --j;
    println( "i: $i, j: $j, k: $k")

}