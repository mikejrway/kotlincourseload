fun main(){
    val list: List<String> = mutableListOf<String>("one", "two", "three")
    val idxTwo = list.indexOf("two")
    val listTwoThree = list.subList(1,3)
    listTwoThree.forEach{print("$it,")} // "two,three."

    println("\n.................")
    val mList = list as MutableList
    mList.add("four")
    list.forEach{print("$it,")} // "one,two,three,four"
}