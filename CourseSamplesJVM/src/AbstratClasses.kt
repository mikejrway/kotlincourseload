abstract class Vehicle {
    abstract fun topSpeed() : Int
}

abstract class Car : Vehicle() {
    //abstract override fun topSpeed() : Int
}

class Model_S : Car() {
    override fun topSpeed() = 152
}