import com.ltree.kotlin.samples.Person
import jdk.nashorn.internal.parser.Token
import kotlinx.coroutines.*
import java.io.File
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousFileChannel
import java.nio.channels.CompletionHandler
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.concurrent.atomic.AtomicLong
import kotlin.concurrent.thread
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import kotlin.system.measureTimeMillis

//val OUTPUT_FILE = """output.txt"""
const val BUFFER_SIZE = 10_000_000
const val FILE_ITERATIONS = 25
val chars = ByteArray(BUFFER_SIZE)
var buffer : ByteBuffer = ByteBuffer.allocate(1)

fun main(){
//    println("Start")
//
//
//    // Start a coroutine
//    GlobalScope.launch {
//        delay(1000)
//        println("Hello")
//    }
//
//    Thread.sleep(2000) // wait for 2 seconds
//    println("Stop")

    //bigCountWithThreads()

    //bigCountWithCoRoutines()

   // bigCountUsingAsync()

    // fill the buffer


    var time = measureTimeMillis {
        runBlocking {
            for( i in 1..5) {
//                launch(newSingleThreadContext("MyOwnThread")){ // thread per coroutine -- all happens in parallel
               launch {
                   //suspendFileWorkLoad("output$i.txt")
                   suspendFileWorkLoadBlockinIO("output$i.txt")
               }
            }
        }
    }

    println("Runtime for coroutines $time")

    time = measureTimeMillis {

        for (i in 6..10) {
            fileWorkLoad("output$i.txt")
        }
    }
    println("Runtime for sequential: $time")
}


/**
 * Demo using threads: takes a long time!
 */
fun bigCountWithThreads(){
    val c = AtomicLong()

    for (i in 1..1_000_000L)
        thread(start = true) {
            c.addAndGet(i)
        }

    println(c.get())
}

fun bigCountWithCoroutines(){

    val c = AtomicLong()

    for (i in 1..1_000_000L)
        GlobalScope.launch {
            c.addAndGet(i)
        }
    // Quick but wrong result as we are not waiting for the coroutines to finish!
    println(c.get())
}


fun bigCountUsingAsync(){
    runBlocking {
    val deferred = (1..1_000_000).map { n ->
        async {
//            delay(1000)
//            n
            workload(n)
        }
    }


        val sum = deferred.sumBy { it.await() }
        println("Sum: $sum")
    }
}

suspend fun workload(n: Int): Int {
    delay(1000)
    return n
}


fun fileWorkLoad(fileName: String) {
    for( idx in 0 until BUFFER_SIZE){
        chars[idx]=0x41
    }

    val data = String(chars)

        val writer = File(fileName).writer()
        for (x in 0..FILE_ITERATIONS) {
            writer.append(data)
        }
        writer.close()

}


/**
 * This version allows us to have non-blocking coroutines beacuse they are actually being run as Threads! Dispatchers.IO
 */
suspend fun suspendFileWorkLoadBlockinIO(fileName: String) {
    for( idx in 0 until BUFFER_SIZE){
        chars[idx]=0x41
    }
    val data = String(chars)
    withContext(Dispatchers.IO) {
        val writer = File(fileName).writer()
        for (x in 0..FILE_ITERATIONS) {
            writer.append(data)
        }
        writer.close()
    }

}

suspend fun suspendFileWorkLoad(fileName: String) {
    for( idx in 0 until BUFFER_SIZE){
        chars[idx]=0x41
    }
    val channel = AsynchronousFileChannel.open(Paths.get(fileName), StandardOpenOption.CREATE, StandardOpenOption.WRITE)
    //val writer = File(fileName).writer()
    channel.use { channel ->
        for (x in 0..FILE_ITERATIONS) {
            // Need to repopulate the buffer before each write
            var buf = ByteBuffer.wrap(chars)
            channel.aWrite(buf, channel.size())
        }
    }

}


/**
 * An extension function for AsynchronousFileChannel
 * It creates a synchronous but suspendable version of the write function by converting the
 * call back functions into triggers that resume the suspended function
 */
suspend fun AsynchronousFileChannel.aWrite(buf: ByteBuffer, position: Long = 0L): Int =
    // The magic: suspendCoroutine suspends the current coroutine until one of the resume functions is called
    suspendCoroutine { cont ->
        write(buf, position, Unit, object : CompletionHandler<Int, Unit> {
            override fun completed(bytesWritten: Int, attachment: Unit) {
                cont.resume(bytesWritten)
            }

            override fun failed(exception: Throwable, attachment: Unit) {
                cont.resumeWithException(exception)
            }
        })
    }

/*
fun fetchLogFilesThreaded(dates: DateRange) {
    val token = requestAuthenticationToken()
    val files = pullLogFilesFromServer(token, dates)
    processLogFiles(files)
}


// Sample of function that uses a call back

fun fetchLogFilesCB(dates: DateRange) {
    requestAuthenticationToken() { token ->
        pullLogFilesFromServer(token, dates) { files ->
            processLogFiles(files)
        }
    }
}


fun fetchLogFilesPromise(dates: DateRange) {
    requestAuthenticationToken()
        .then { token ->
            pullLogFilesFromServer(token, dates)
        }
        .then { files ->
            processLogFiles(files)
        }
}

fun fetchLogFilesObs(dates: DateRange) {
    requestAuthenticationToken().subscribe {
        pullLogFilesFromServer(it, dates).subscribe {
            processLogFiles(it)
        }}
}

fun preparePostAsync(): Promise<Token> {
    // makes request an returns a promise that is completed later
    return promise
}
fun requestAuthenticationToken(callback: (Token) -> Unit) {
    // make request and return immediately
    // arrange callback to be invoked later
}

fun pullLogFilesFromServer(token: Token, dates: DateRange, callback: (files: LogFiles) -> Unit) : Unit{

}

fun processLogFiles(post: Any){

}

fun main2() {

    doSomethingLater(42, {res: String -> println(res)})
}


fun main3() {
        doSomethingLater(42, {res: String -> {
            println(res)
            doSomethingLater(42, {res : String ->
                println(res)
            })}})
}

fun doSomethingLater(input: Int, callback:(result: String) -> Unit){
    val result = getDataFromServer()
    callback(result)
}


fun getDataFromServer(): String = "Some data"
*/

/**
        To make this work you need this MVN dep
        <dependency>
        <groupId>org.jetbrains.kotlinx</groupId>
        <artifactId>kotlinx-coroutines-core</artifactId>
        <version>1.0.1</version>
        </dependency>
        */


