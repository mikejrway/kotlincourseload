fun main(){
    simpleStringTest()
    rawStringTest()
    rawStringWithDollar()
    escapedString()
    escapedStringWithDollarLiteral()
}

fun simpleStringTest(){
    val aString = "Simple String"
    println(aString)
}

fun rawStringTest(){
    // Note the | chars being used to define a margin
    val aString = """
        |Here is
        |some stuff
            |that should print with new line chars
                |and is badly laid out in the code
    """.trimMargin()
    println(aString)
}

fun rawStringWithDollar(){
    val aString = """

        Here is some text with a ${'$'}x $99.99 character""".trimIndent()
    println(aString)
}


fun escapedString(){
    val x=4
    val aString= "\n\nString with a $x"
    println(aString)
}

fun escapedStringWithDollarLiteral(){
    val x=4 // Unused
    val aString= "\n\nString with a $99.99" // Prints as "String with a $x"
    println(aString)
}