#Notes on the 2327 Course Load

##The Web Service
There is a Web service running on port 9090. It is created from the CaseStudyWebApp project (it is actually a Spring project).
To create the service:
* Run the spring-boot:run goal from Maven
* Once started run the package goal from Maven
* Copy the resultant Jar from C:\course2327\CaseStudyWebApp\target to C:\course2327\bin
* Restart the service: CaseStudy2327

This service should be replaced by the main JVM jar from the CaseStudyMPP project
(need to add a manifest and test it...) 
